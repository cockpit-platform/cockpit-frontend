/*

// boilerplate of angular2-jwt / angular2-jwt-refresh
import { NgModule } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { AuthConfig } from 'angular2-jwt';
import { JwtConfigService, JwtHttp } from 'angular2-jwt-refresh';
import {environment} from '../environments/environment';


@NgModule({
  providers: [{
    provide: JwtHttp,
    useFactory: getJwtHttp,
    deps: [ HttpClient ]
  }]
})
export class AuthModule {}

export function getJwtHttp(http: HttpClient) {
  const jwtOptions = {
    endPoint: environment['serverBaseUrl'] + 'api-token-refresh/',
    // optional
    // payload: { token: token },
    payload: () => {return new Promise((resolve, reject) => {
      resolve({token: localStorage.getItem('token')});
      reject({token: ''}); }
    )},
    beforeSeconds: 300, // refresh tokeSn before 5 min
    tokenName: 'token',
    refreshTokenGetter: (() => localStorage.getItem('token')),
    tokenSetter: ((res: Response): boolean | Promise<void> => {
      res = res.json();

      if (!('token' in res)) {
        console.log('ping');
        localStorage.removeItem('token');

        return false;
      }

      localStorage.setItem('token', res['token']);

      return true;
    })
  };
  const authConfig = new AuthConfig({
    tokenName: 'token',
    headerPrefix: 'JWT',
    noJwtError: true,
    globalHeaders: [{'Accept': 'application/json'}],
    tokenGetter: (() => localStorage.getItem('token')),
  });

  return new JwtHttp(
    new JwtConfigService(jwtOptions, authConfig),
    http
  );
}
//
// import { NgModule } from '@angular/core';
// import { Http, RequestOptions } from '@angular/http';
// import { AuthHttp, AuthConfig } from 'angular2-jwt';
//
// export function authHttpServiceFactory(http: Http, options: RequestOptions) {
//   return new AuthHttp(new AuthConfig({
//     tokenName: 'token',
//     headerPrefix: 'JWT',
//     tokenGetter: (() => sessionStorage.getItem('token')),
//     globalHeaders: [{'Content-Type': 'application/json'}],
//   }), http, options);
// }
//
// @NgModule({
//   providers: [
//     {
//       provide: AuthHttp,
//       useFactory: authHttpServiceFactory,
//       deps: [Http, RequestOptions]
//     }
//   ]
// })
// export class AuthModule {}

*/
