import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillAvailabilityLookupComponent } from './skill-availability-lookup.component';

describe('SkillAvailabilityLookupComponent', () => {
  let component: SkillAvailabilityLookupComponent;
  let fixture: ComponentFixture<SkillAvailabilityLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillAvailabilityLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillAvailabilityLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
