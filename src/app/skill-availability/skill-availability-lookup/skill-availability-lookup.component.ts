import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProfilesDataService } from '../../shared/profiles-data.service';
import { Subscription } from 'rxjs';
import {
  Skill,
  AvailabilitySearch,
  Availability,
} from 'app/skill-availability/state/models';
import { User } from 'app/shared/model/models';
import { SkillAvailabilityService } from 'app/skill-availability/state/skill-availability.service';

export interface GroupedSkill {
  name: string;
  users: [
    {
      user_url: string;
      user_name: string;
      knowledge_level: string;
      motivation_level: string;
    }
  ];
}

export interface GroupedAvailability {
  user_url: string;
  user_name: string;
  availabilities: [
    {
      available_from: Date;
      available_to: Date;
      working_hours_day: number;
    }
  ];
}

@Component({
  selector: 'app-skill-lookup',
  templateUrl: './skill-availability-lookup.component.html',
  styleUrls: ['./skill-availability-lookup.component.scss'],
})
export class SkillAvailabilityLookupComponent implements OnInit, OnDestroy {
  errorMessage: string;
  skillName: string;
  fromDate: Date;
  toDate: Date;
  groupedSkills: GroupedSkill[];
  groupedAvailabilities: AvailabilitySearch[];
  groupedSkillsAndAvailabilities: any;
  usersSub: Subscription;
  users: User[];
  skillSearched: boolean;
  availabilitySearched: boolean;
  skillColumnsToDisplay = ['user', 'knowledge_level', 'motivation_level'];
  mergedAvailabilities: object;

  constructor(
    private skillAvailabilityService: SkillAvailabilityService,
    private profilesDataService: ProfilesDataService
  ) {}

  ngOnInit() {}

  ngOnDestroy() {
    if (this.usersSub) {
      this.usersSub.unsubscribe();
    }
  }

  /**
   * The form was submitted - lookup the skill searched for
   */
  onSubmit() {
    this.skillSearched = false;
    this.availabilitySearched = false;

    if (this.skillName) {
      this.skillSearched = true;
    }
    if (this.fromDate && this.toDate) {
      this.availabilitySearched = true;
    }

    if (this.skillSearched && this.availabilitySearched) {
      this.groupedSkillsAndAvailabilities = [];
      this.skillAvailabilityService.lookupSkill(this.skillName).subscribe(
        res => {
          this.groupedSkills = this.groupSkills(res);
          const userUrls = res
            .map(entry => entry.user)
            .filter((v, i, a) => a.indexOf(v) === i);
          this.skillAvailabilityService
            .getAvailabilitiesForUsers(this.fromDate, this.toDate, userUrls)
            .subscribe(
              res2 => {
                this.groupedAvailabilities = res2;
                this.groupedSkills.forEach(skill => {
                  const users = skill.users.map(user => user.user_url);
                  this.groupedSkillsAndAvailabilities.push({
                    skill: skill,
                    availabilities: this.mergeAvailabilities(
                      res2.filter(user => users.includes(user.user)),
                      skill.users
                    ),
                  });
                });
              },
              error => {
                this.errorMessage = error;
              }
            );
        },
        error => {
          this.errorMessage = error;
        }
      );
    } else if (this.skillSearched) {
      this.skillAvailabilityService.lookupSkill(this.skillName).subscribe(
        res => {
          this.groupedSkills = this.groupSkills(res);
        },
        error => {
          this.errorMessage = error;
        }
      );
    }
  }

  resetFormValues() {
    this.skillName = '';
    this.fromDate = null;
    this.toDate = null;
  }

  mergeAvailabilities(availabilities: AvailabilitySearch[], users: any[]) {
    const mergedAvailabilities = {};

    availabilities.forEach(availability => {
      availability.days.forEach(day => {
        if (mergedAvailabilities.hasOwnProperty(day.day)) {
          mergedAvailabilities[day.day].users.push({
            user: users.find(user => user.user_url === availability.user),
            working_hours_day: day.working_hours_day,
          });
          mergedAvailabilities[day.day]['working_hours_day_sum'] +=
            day.working_hours_day;
        } else {
          mergedAvailabilities[day.day] = {
            users: [
              {
                user: users.find(user => user.user_url === availability.user),
                working_hours_day: day.working_hours_day,
              },
            ],
            working_hours_day_sum: day.working_hours_day,
          };
        }
      });
    });

    return mergedAvailabilities;
  }

  /**
   * Group the array of skills returned from the server to display a list of skills and the users possessing them
   * @param skills
   */
  groupSkills(skills: Skill[]): GroupedSkill[] {
    const groupedSkills = [];
    let index = 0;

    // Fill array with grouped skills and remove from skills array accordingly until the latter is empty
    while (skills.length > 0) {
      const skillName = skills[0].skilltag_set[0].name; // Name of the skill we are grouping
      groupedSkills.push({
        name: skillName,
        users: [],
      });

      // Go through the results, if skillName is found somewhere else, add this user details to groupedSkills
      skills.forEach(element => {
        if (element.skilltag_set[0].name === skillName) {
          groupedSkills[index].users.push({
            user_url: element.user,
            user_name: '',
            knowledge_level: element.knowledge_level,
            motivation_level: element.motivation_level,
          });
        }
      });

      // Now remove all elements with skillName from the skills array
      skills = skills.filter(
        element => element.skilltag_set[0].name !== skillName
      );

      index++;
    }

    // Load the user details of each user_url in the groupedSkills-array into the store
    groupedSkills.forEach(element => {
      element.users.forEach(element2 => {
        this.profilesDataService.loadUserDetails(element2.user_url);
      });
    });

    this.usersSub = this.profilesDataService.users.subscribe(res => {
      this.users = res;

      // For each user_url in groupedSkill, search this user url in the user-details-array from the server
      groupedSkills.forEach(groupedSkill => {
        groupedSkill.users.forEach(groupedSkillUser => {
          // Find and return username for user_url
          groupedSkillUser.user_name = this.users.find(user => {
            return groupedSkillUser.user_url === user.url;
          }).username;
        });
      });
    });

    return groupedSkills;
  }

}
