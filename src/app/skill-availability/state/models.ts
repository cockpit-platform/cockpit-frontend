export interface SkillTag {
  name: string;
}

export interface Skill {
  knowledge_level: 'basic' | 'good' | 'high' | 'expert';
  motivation_level: 'ok' | 'love' | 'wants' | 'boring';
  skilltag_set: SkillTag[];
  url: string;
  user: string;
}

export interface Availability {
  date_range: string;
  url: string;
  user: string;
  working_hours_day: number;
}
export interface AvailabilitySearch {
  // date_range: string;
  days: AvailabilitySearchDay[];
  url: string;
  user: string;
  working_hours_day: number;
}

export interface AvailabilitySearchDay {
  day: string;
  working_hours_day: number;
}

export interface AvailabilityReadable {
  available_from: Date;
  available_to: Date;
  url: string;
  user: string;
  working_hours_day: number;
}
