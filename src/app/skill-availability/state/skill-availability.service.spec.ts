import { TestBed } from '@angular/core/testing';

import { SkillAvailabilityService } from './skill-availability.service';

describe('SkillDataServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SkillAvailabilityService = TestBed.get(SkillAvailabilityService);
    expect(service).toBeTruthy();
  });
});
