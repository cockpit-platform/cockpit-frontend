import {Injectable} from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable, throwError, merge, forkJoin, combineLatest, zip} from 'rxjs';
import {catchError, combineAll, flatMap, mergeAll} from 'rxjs/operators';
import {Availability, AvailabilitySearch, AvailabilityReadable, Skill, SkillTag} from 'app/skill-availability/state/models';

const SKILL_API_URL = environment['serverBaseUrl'] + 'skillmanagement/skills/';
const SKILL_API_URL_USER = SKILL_API_URL + 'user/';
const SKILL_API_URL_SEARCH = SKILL_API_URL + 'lookup/';
const SKILL_API_URL_SKILLTAGS = SKILL_API_URL + 'taglist/';
const AVAILABILITY_API_URL = environment['serverBaseUrl'] + 'skillmanagement/availability/';
const AVAILABILITY_API_URL_USER = AVAILABILITY_API_URL + 'user/';

@Injectable({
  providedIn: 'root'
})
export class SkillAvailabilityService {

  constructor(private http: HttpClient) {
  }

  private static convertDatesToDaterange(dateLower: Date, dateUpper: Date): string {
       return '{"bounds": "[)", "lower": "' + SkillAvailabilityService.dateToString(dateLower) +
        '", "upper": "' + SkillAvailabilityService.dateToString(dateUpper) + '"}';
  }

  /**
   * Takes an Date object and returns a date string in format yyyy-mm-dd
   * @param date
   * @return string
   */
  private static dateToString(date: Date): string {
    let dateString: string;
    dateString = date.getFullYear().toString() + '-';

    if (date.getMonth() < 9) {
      dateString += '0' + ( date.getMonth() + 1 ).toString() + '-';
    } else {
      dateString += ( date.getMonth() + 1).toString() + '-';
    }

    if (date.getDate() < 10) {
      dateString += '0' + date.getDate().toString();
    } else {
      dateString += date.getDate().toString();
    }

    return dateString;
  }

  /**
   * Extract the "from" date from the date_range field of json from backend
   * @param dateString
   * @return string
   */
  private static extractLowerDate(dateString: string): string {
    const regExp = '\\d{4}-\\d{2}-\\d{2}';
    return dateString.substr(dateString.search(regExp), 10);
  }

  /**
   * Extract the "to" date from the date_range field of json from backend
   * @param dateString
   * @return string
   */
  private static extractUpperDate(dateString: string): string {
    const regExp = '\\d{4}-\\d{2}-\\d{2}';
    // Remove the lower date string first
    dateString = dateString.replace(SkillAvailabilityService.extractLowerDate(dateString), '');
    return dateString.substr(dateString.search(regExp), 10);
  }

  /**
   * Handle frontend and backend errors
   * @param error
   * @return Observable<never>
   */
  private static handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
      return throwError(
        'Client or network error occurred, see console output.');
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      return throwError(
        `A backend error occurred: ${error.status} ${error.error}`);
    }
    // return an observable with a user-facing error message
  }

  /**
   * Takes an url and returns the value before the trailing slash
   * @param url
   * @return string
   */
  public static extractLastElementFromUrl(url: string): string {
    // Remove trailing '/' from url
    url = url.substring(0, url.length - 1);
    // Extract value after the last /
    return url.substring(url.lastIndexOf('/') + 1);
  }

  private static setHttpOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'JWT ' + localStorage.getItem('token')
      })
    };
  }

  /**
   * Get all skills associated with a user
   * @param userUrl
   * @return Observable
   */
  getSkillsForUser(userUrl: string): Observable<Skill[]> {
    let user_id: String;
    user_id = SkillAvailabilityService.extractLastElementFromUrl(userUrl);
    return this.http.get<Skill[]>(SKILL_API_URL_USER + user_id).pipe(catchError(SkillAvailabilityService.handleError));
  }

  /**
   * Search for skill name
   * @param skillName
   * @return Observable
   */
  lookupSkill(skillName: string): Observable<Skill[]> {
    return this.http.get<Skill[]>(SKILL_API_URL_SEARCH + skillName).pipe(catchError(SkillAvailabilityService.handleError));
  }

  /**
   * Get all available skill tags
   * @return Observable
   */
  getSkillTags(): Observable<SkillTag[]> {
    return this.http.get<SkillTag[]>(SKILL_API_URL_SKILLTAGS).pipe(catchError(SkillAvailabilityService.handleError));
  }

  /**
   * Create a skill for a specific user
   * @param skill
   * @param userUrl
   */
  createSkill(skill: any, userUrl: string): Observable<Skill> {
    const httpOptions = SkillAvailabilityService.setHttpOptions();

    const postJson = {
      'skilltag_set': [{'skill': skill.skillName, 'name': skill.skillName}],
      'knowledge_level': skill.skillKnowledge,
      'motivation_level': skill.skillMotivation,
      'user': userUrl
    };

    return this.http.post<Skill>(SKILL_API_URL, JSON.stringify(postJson), httpOptions).pipe(catchError(SkillAvailabilityService.handleError));
  }

  /**
   * Delete a specific skill
   * @param skillUrl
   */
  deleteSkill(skillUrl: string): Observable<Skill> {
    const httpOptions = SkillAvailabilityService.setHttpOptions();

    const skillId = SkillAvailabilityService.extractLastElementFromUrl(skillUrl);

    return this.http.delete<Skill>(SKILL_API_URL + skillId, httpOptions).pipe(catchError(SkillAvailabilityService.handleError));
  }

  /**
   * Update a skill
   * @param skill
   */
  updateSkill(skill): Observable<Skill> {
    const httpOptions = SkillAvailabilityService.setHttpOptions();

    // todo (tbd) should be possible to patch skilltag-name...
    const skillId = SkillAvailabilityService.extractLastElementFromUrl(skill.skillUrl);
    const postJson = {
      'knowledge_level': skill.skillKnowledge,
      'motivation_level': skill.skillMotivation
    };

    return this.http.patch<Skill>(SKILL_API_URL + skillId + '/', JSON.stringify(postJson), httpOptions).pipe(catchError(SkillAvailabilityService.handleError));
  }

  /**
   * Get all availabilities for specific user
   * @param userUrl
   */
  getAvailabilitiesForUser(userUrl: string): Observable<Availability[]> {
    let user_id: String;
    user_id = SkillAvailabilityService.extractLastElementFromUrl(userUrl);
    return this.http.get<Availability[]>(AVAILABILITY_API_URL_USER + user_id).pipe(catchError(SkillAvailabilityService.handleError));
  }

  getOverlappingAvailabilitiesForDaterange(fromDate: Date, toDate: Date): Observable<Availability[]> {
    console.log(fromDate.toDateString());
    const dateString = SkillAvailabilityService.dateToString(fromDate) + '/' + SkillAvailabilityService.dateToString(toDate);
    return this.http.get<Availability[]>(AVAILABILITY_API_URL + dateString).pipe(catchError(SkillAvailabilityService.handleError));
  }

  getAvailabilitiesForUsers(fromDate: Date, toDate: Date, userUrls: string[]): Observable<AvailabilitySearch[]> {
    const dateString = SkillAvailabilityService.dateToString(fromDate) + '/' + SkillAvailabilityService.dateToString(toDate) + '/';
    const requests = userUrls.map(userUrl => {
      const user_id = SkillAvailabilityService.extractLastElementFromUrl(userUrl);
      return this.http.get<AvailabilitySearch>(AVAILABILITY_API_URL + user_id + '/' + dateString).pipe(catchError(SkillAvailabilityService.handleError));
    });

    return forkJoin(requests);
  }

  /**
   * Create an availability
   * @param availabilityReadable
   * @param userUrl
   */
  createAvailability(availabilityReadable: AvailabilityReadable, userUrl: string): Observable<Availability> {
    const httpOptions = SkillAvailabilityService.setHttpOptions();
    const dateRange = SkillAvailabilityService.convertDatesToDaterange(availabilityReadable.available_from, availabilityReadable.available_to);

    const postJson = {
      'user': userUrl,
      'working_hours_day': availabilityReadable.working_hours_day,
      'date_range': dateRange
    };

    return this.http.post<Availability>(AVAILABILITY_API_URL, JSON.stringify(postJson), httpOptions).pipe(catchError(SkillAvailabilityService.handleError));
  }

  /**
   * Delete an availability
   * @param availabilityUrl
   */
  deleteAvailability(availabilityUrl: string): Observable<Availability> {
    const httpOptions = SkillAvailabilityService.setHttpOptions();

    const availabilityId = SkillAvailabilityService.extractLastElementFromUrl(availabilityUrl);

    return this.http.delete<Availability>(AVAILABILITY_API_URL + availabilityId, httpOptions).pipe(catchError(SkillAvailabilityService.handleError));
  }

  /**
   * Convert availability from backend format into a more readable format using Date for dates
   * @param availabilityFromBackend
   * @return AvailabilityReadable[]
   */
  makeAvailabilitiesReadable(availabilityFromBackend: Availability[]): AvailabilityReadable[] {
    let availabilityReadable: AvailabilityReadable[];
    availabilityReadable = [];
    console.log(availabilityFromBackend);
    availabilityFromBackend.forEach(availability => {
      const lowerDate = SkillAvailabilityService.extractLowerDate(availability.date_range);
      const upperDate = SkillAvailabilityService.extractUpperDate(availability.date_range);
      availabilityReadable.push({
        available_from: new Date(lowerDate),
        available_to: new Date(upperDate),
        // available_from: {
          // year: parseInt(lowerDate.substr(0, 4)),
          // month: parseInt(lowerDate.substr(5, 2)),
          // day: parseInt(lowerDate.substr(8, 2)),
        // },
        // available_to: {
          // year: parseInt(upperDate.substr(0, 4)),
          // month: parseInt(upperDate.substr(5, 2)),
          // day: parseInt(upperDate.substr(8, 2)),
        // },
        url: availability.url,
        user: availability.user,
        working_hours_day: availability.working_hours_day
      });
    });
    return availabilityReadable;
  }

  /**
   * Sort availabilities chronologically based on "available from" date
   * @param availabilities
   */
  sortAvailabilities(availabilities: AvailabilityReadable[]): AvailabilityReadable[] {
    availabilities = availabilities.sort((n1, n2) => {
      if (n1.available_from > n2.available_from) {
        return 1;
      } else if (n1.available_from < n2.available_from) {
        return -1;
      } else {
        return 0;
      }
    });
    return availabilities;
  }

}
