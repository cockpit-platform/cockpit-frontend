import {Component, OnInit} from '@angular/core';
import {ProfilesDataService} from '../../shared/profiles-data.service';
import {Subscription} from 'rxjs';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {SkillAvailabilityService} from 'app/skill-availability/state/skill-availability.service';
import {AvailabilityReadable} from 'app/skill-availability/state/models';
import {User} from 'app/shared/model/models';
import {Router} from '@angular/router';

@Component({
  selector: 'app-availability-list',
  templateUrl: './availability-user.component.html',
  styleUrls: ['./availability-user.component.scss']
})

export class AvailabilityUserComponent implements OnInit {

  profileUserUrl: string;
  errorMessage: string;
  userAvailabilities: AvailabilityReadable[];
  editMode: boolean;
  authSub: Subscription;
  isMyProfilePage: boolean;
  loggedInUserUrl: string;
  availabilityForm: FormArray;
  formValidated: boolean;
  workingHoursOptions: number[];
  displayedColumns: string[] = ['from', 'to', 'working_hours_day'];

  constructor(private skillDataService: SkillAvailabilityService, private profilesDataService: ProfilesDataService,
              private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.editMode = false;
    this.workingHoursOptions = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    this.getUserUrl(this.router.url);

    this.getAvailabilitiesAndInitForm();
  }

  getAvailabilitiesAndInitForm() {
    this.skillDataService.getAvailabilitiesForUser(this.profileUserUrl).subscribe(response => {
      this.userAvailabilities = this.skillDataService.sortAvailabilities(this.skillDataService.makeAvailabilitiesReadable(response));

      // Check if we are on the profile page of the user logged in
      this.authSub = this.profilesDataService.auth.subscribe(auth => {
        this.isMyProfilePage = auth.user.url === this.profileUserUrl;
        this.loggedInUserUrl = auth.user.url;
      });

      if (this.isMyProfilePage) {
        this.fillFormArray();
      }
    });
  }

  fillFormArray() {
    this.availabilityForm = this.fb.array([]);
    this.userAvailabilities.forEach(availability => {
      this.availabilityForm.push(this.createFormGroup(availability));
    });
  }

  createFormGroup(availability?: AvailabilityReadable): FormGroup {
    if (availability) {
      return this.fb.group({
        available_from: [availability.available_from],
        available_to: [availability.available_to],
        url: [availability.url],
        user: [availability.user],
        working_hours_day: [availability.working_hours_day]
      });
    } else {
      return this.fb.group({
        available_from: [''],
        available_to: [''],
        url: [''],
        user: [''],
        working_hours_day: ['']
      });
    }
  }

  onSubmit() {
    // Only do something when a value has changed
    if (this.availabilityForm.pristine) {
      return;
    }

    // Form is invalid, activates was-validated class
    if (!this.availabilityForm.valid) {
      this.formValidated = true;
      return;
    }

    // Loop through all the rows in the form and add or update availabilities
    this.availabilityForm.controls.forEach(availabilityFormElement => {
      if (!availabilityFormElement.pristine) {
        // We always recreate and don't update/patch the availability because backend does conflict resolution
        this.skillDataService.createAvailability(availabilityFormElement.value, this.loggedInUserUrl).subscribe(null, error => {
          this.errorMessage = error;
        });
      }
    });

    // Loop through all the availabilities originally loaded from server and check if it exists in the form
    // If not: delete it
    this.userAvailabilities.forEach(userAvailabilityElement => {
      if (!this.availabilityForm.controls.some
      (availabilityFormElement => availabilityFormElement.value.url === userAvailabilityElement.url)) {
        this.skillDataService.deleteAvailability(userAvailabilityElement.url).subscribe(null, error => {
          this.errorMessage = error;
        });
      }
    });

    this.formValidated = false;
    this.editMode = false;

    // Reload user availabilities from server
    // We load two times because first response from server is incoherent
    // todo tbd jannis this sucks of course. how can this be fixed?
    this.getAvailabilitiesAndInitForm();
    this.getAvailabilitiesAndInitForm();
  }

  deleteAvailability(arrayPosition: number) {
    this.availabilityForm.removeAt(arrayPosition);
    this.availabilityForm.markAsDirty();
  }

  addAvailability() {
    this.availabilityForm.push(this.createFormGroup());
  }

  resetFormValues() {
    this.formValidated = false;
    this.editMode = false;
    this.fillFormArray();
  }

  getUserUrl(currentUrl: string) {
    let user: User;
    this.profilesDataService.users.subscribe(users => {
      if (users.length > 0) {
        user = users.find(user => {
          // Get the user with the username of the current url
          return user.username === currentUrl.substring(currentUrl.lastIndexOf('/') + 1);
        });
      }
      this.profileUserUrl = user.url;
    });
  }

}
