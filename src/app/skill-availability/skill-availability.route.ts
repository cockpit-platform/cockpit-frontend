import {Route} from '@angular/router';
import {AppContainerComponent} from 'app/app-container/app-container.component';
import {AuthGuard} from 'app/shared/profiles-data.service';
import {SkillAvailabilityLookupComponent} from 'app/skill-availability/skill-availability-lookup/skill-availability-lookup.component';
import {SkillUserComponent} from 'app/skill-availability/skill-user/skill-user.component';

export const SKILL_AVAILABILITY_ROUTE: Route = {
  path: 'app',
  component: AppContainerComponent,
  canActivate: [AuthGuard],
  children: [ {
    path: 'skill-availability-lookup',
    component: SkillAvailabilityLookupComponent,
  } ]
};
