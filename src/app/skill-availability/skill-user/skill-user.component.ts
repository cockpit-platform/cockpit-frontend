import {Component, OnInit, OnDestroy} from '@angular/core';
import {ProfilesDataService} from '../../shared/profiles-data.service';
import {FormGroup, FormBuilder, FormArray, Validators} from '@angular/forms';
import {Observable, Subscription, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {Skill, SkillTag} from 'app/skill-availability/state/models';
import {SkillAvailabilityService} from 'app/skill-availability/state/skill-availability.service';
import {Router} from '@angular/router';
import {User} from 'app/shared/model/models';

@Component({
  selector: 'app-skill-list',
  templateUrl: './skill-user.component.html',
  styleUrls: ['./skill-user.component.scss']
})

export class SkillUserComponent implements OnInit, OnDestroy {

  profileUserUrl: string;
  userSkills: Skill[];
  skillTags: string[];
  isMyProfilePage: boolean;
  authSub: Subscription;
  loggedInUserUrl: string;
  skillEditMode: boolean;
  skill: Skill;
  skillForm: FormArray;
  formValidated: boolean;
  errorMessage: string;

  // todo tbd (jannis) this should be defined in backend?
  knowledgeOptions = ['basic', 'good', 'high', 'expert'];
  motivationOptions = ['boring', 'ok', 'wants', 'love'];

  constructor(private profilesDataService: ProfilesDataService, private skillAvailabilityService: SkillAvailabilityService,
              private fb: FormBuilder, private router: Router) {
  }

  // Skill name autocomplete
  skillsFiltered: Observable<string[]>;
  skillNameSubject = new Subject<string>();

  ngOnInit() {
    this.getUserUrl(this.router.url);
    this.skillEditMode = false;

    // Check if we are on the profile page of the user logged in
    this.authSub = this.profilesDataService.auth.subscribe(auth => {
      this.isMyProfilePage = auth.user.url === this.profileUserUrl;
      this.loggedInUserUrl = auth.user.url;
    });

    // Get user skills and, if we are on the profile page of the logged in user, init form data
    this.getUserSkillsAndInitForm();

    // We need skill tags only if we can add/edit the skills
    if (this.isMyProfilePage) {
      this.getSkillTags();
    }

    // Skill name autocomplete
    this.skillsFiltered = this.skillNameSubject.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.skillTags.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );
  }

  ngOnDestroy() {
    this.authSub.unsubscribe();
  }

  /**
   * Return a form group with the values of given skill object or add an empty one for the user to fill out
   * @param skill - A skill object. If none passed, initialize empty (new row in form)
   */
  createFormGroup(skill?: Skill): FormGroup {
    if (skill) {
      return this.fb.group({
        skillUrl: [skill.url],
        skillName: [skill.skilltag_set[0].name, [Validators.required, Validators.maxLength(20)]],
        skillKnowledge: [skill.knowledge_level, Validators.required],
        skillMotivation: [skill.motivation_level, Validators.required],
      });
    } else {
      return this.fb.group({
        skillUrl: [''],
        skillName: ['', [Validators.required, Validators.maxLength(20)]],
        skillKnowledge: ['', Validators.required],
        skillMotivation: ['', Validators.required],
      });
    }
  }

  /**
   * Fill the form groups array with the values of the skills of the user
   */
  fillFormArray() {
    // Initialize the form array
    this.skillForm = this.fb.array([]);

    // Fill the form array with the skills of the user
    this.userSkills.forEach(skill => {
      this.skillForm.push(this.createFormGroup(skill));
    });
  }

  /**
   * The form has been submitted - now update server data accordingly
   */
  onSubmit() {
    // Only do something when a value has changed
    if (this.skillForm.pristine) {
      return;
    }

    // Form is invalid, activates was-validated class
    if (!this.skillForm.valid) {
      this.formValidated = true;
      return;
    }

    // Loop through all the rows in the form and add or update skills
    this.skillForm.controls.forEach(skillFormElement => {
      if (!skillFormElement.pristine) {
        if (skillFormElement.value.skillUrl !== '') { // Not empty skillUrl means the skill existed on form creation
          if (skillFormElement['controls'].skillName.pristine) { // The skill-name did not change, we can use updateSkill
            this.skillAvailabilityService.updateSkill(skillFormElement.value).subscribe(null, error => {
              this.errorMessage = error;
            });
          } else { // The skill name changed, we need to delete and recreate skill (workaround)
            // todo tbd the backend does not allow to update to name of a skilltag, only to add new links to other skill-tags
            // therefore here if we want to update the name of a skill-tag, we delete it and then recreate with new values
            this.skillAvailabilityService.deleteSkill(skillFormElement.value.skillUrl).subscribe(null, error => {
              this.errorMessage = error;
            });
            this.skillAvailabilityService.createSkill(skillFormElement.value, this.loggedInUserUrl).subscribe(null, error => {
              this.errorMessage = error;
            });
          }
        } else { // Empty skillUrl means it's a new skill
          this.skillAvailabilityService.createSkill(skillFormElement.value, this.loggedInUserUrl).subscribe(null, error => {
            this.errorMessage = error;
          });
        }
      }
    });

    // Loop through all the skills originally loaded from server and check if it exists in the form
    // If not: delete it
    this.userSkills.forEach(userSkillElement => {
      if (!this.skillForm.controls.some
      (skillFormElement => skillFormElement.value.skillUrl === userSkillElement.url)) {
        this.skillAvailabilityService.deleteSkill(userSkillElement.url).subscribe(null, error => {
          this.errorMessage = error;
        });
      }
    });

    this.formValidated = false;
    this.skillEditMode = false;

    // Reload user skills from server
    // We load two times because first response from server is incoherent after skill delete
    // todo tbd jannis this sucks of course. how can this be fixed?
    this.getUserSkillsAndInitForm();
    this.getUserSkillsAndInitForm();
  }

  /**
   * Deletes a skill from formArray at given position
   * @param arrayPosition
   */
  deleteSkill(arrayPosition: number) {
    this.skillForm.removeAt(arrayPosition);
    this.skillForm.markAsDirty();
  }

  /**
   * Add an empty skill-entry to formArray
   */
  addSkill() {
    this.skillForm.push(this.createFormGroup());
    this.skillForm.markAsDirty();
  }

  /**
   * User clicked cancel-button - reset form
   */
  resetFormValues() {
    this.formValidated = false;
    this.skillEditMode = false;
    this.fillFormArray();
  }

  /**
   * Get all the skills of the user of the profile page we are looking at
   * If this is the page of the user logged in (and hence editable), init the form data
   */
  getUserSkillsAndInitForm() {
    // Get the skills for the user of the profile we are looking at from the service
    this.skillAvailabilityService.getSkillsForUser(this.profileUserUrl).subscribe((data: Skill[]) => {
        this.userSkills = data;
        // Initialize form stuff only if we need it
        if (this.isMyProfilePage) {
          this.fillFormArray();
        }
      },
      error => {
        this.errorMessage = error;
      }
    );
  }

  /**
   * Get all the skill tags existing. Needed for typeahead in the form
   */
  getSkillTags() {
    this.skillAvailabilityService.getSkillTags().subscribe((res: SkillTag[]) => {
        this.skillTags = res.map(element => {
          return element.name;
        });
      },
      error => {
        this.errorMessage = error;
      }
    );
  }

  getUserUrl(currentUrl: string) {
    let user: User;
    this.profilesDataService.users.subscribe(users => {
      if (users.length > 0) {
        user = users.find(user => {
          // Get the user with the username of the current url
          return user.username === currentUrl.substring(currentUrl.lastIndexOf('/') + 1);
        });
      }
      this.profileUserUrl = user.url;
    });
  }

  /**
   * Event for skill name fields to update autocomplete list
   */
  skillNameChanged(value: string) {
    this.skillNameSubject.next(value);
  }
}
