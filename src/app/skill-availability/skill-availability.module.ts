import {LOCALE_ID, NgModule} from '@angular/core';
import {CommonModule, registerLocaleData} from '@angular/common';

import { SkillAvailabilityLookupComponent } from './skill-availability-lookup/skill-availability-lookup.component';
import {RouterModule} from '@angular/router';
import {PluginConfig, PluginPlacement} from 'app/plugin/plugin';
import {PluginService} from 'app/plugin/plugin-service';
import {SKILL_AVAILABILITY_ROUTE} from 'app/skill-availability/skill-availability.route';
import { NavigationComponent } from './navigation/navigation.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SkillUserComponent} from 'app/skill-availability/skill-user/skill-user.component';
import {AvailabilityUserComponent} from 'app/skill-availability/availability-user/availability-user.component';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {GermanKnowledgeMotivationOptionsPipe} from 'app/skill-availability/german-knowledge-motivation.pipe';
import localeDe from '@angular/common/locales/de';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatTableModule} from '@angular/material/table';
import {MatSelectModule} from '@angular/material/select';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCardModule} from '@angular/material/card';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([SKILL_AVAILABILITY_ROUTE]),
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatListModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatChipsModule,
    MatAutocompleteModule,
    FlexLayoutModule,
    MatTableModule,
    MatTooltipModule,
    MatCardModule,
  ],
  exports: [RouterModule],
  declarations: [
    SkillAvailabilityLookupComponent,
    SkillUserComponent,
    AvailabilityUserComponent,
    NavigationComponent,
    GermanKnowledgeMotivationOptionsPipe
  ],
  entryComponents: [
    SkillAvailabilityLookupComponent,
    SkillUserComponent,
    AvailabilityUserComponent,
    NavigationComponent
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'de-DE' }
  ]
})

@PluginConfig({
  name: 'skill-availability',
  description:
    'Manage users skills and availability',
  placements: [
    new PluginPlacement({
      slot: 'profile',
      priority: 1,
      component: SkillUserComponent,
    }),
    new PluginPlacement({
      slot: 'profile',
      priority: 1,
      component: AvailabilityUserComponent,
    }),
    new PluginPlacement({
      slot: 'navigation',
      priority: 1,
      component: NavigationComponent,
    })
  ],
})

export class SkillAvailabilityModule {
  constructor(private pluginService: PluginService) {
    this.pluginService.registerPlugin(this);
    registerLocaleData(localeDe);
  }
}
