import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'germanKnowledgeMotivationOptions'})
export class GermanKnowledgeMotivationOptionsPipe implements PipeTransform {
  transform(english: string): string {
    switch (english) {
      case 'basic':
        return 'grundlegend';
      case 'good':
        return 'gut';
      case 'high':
        return 'hoch';
      case 'expert':
        return 'expert*in';
      case 'boring':
        return 'langweilig';
      case 'ok':
        return 'ok';
      case 'wants':
        return 'gerne';
      case 'love':
        return 'liebe es';
    }
  }
}
