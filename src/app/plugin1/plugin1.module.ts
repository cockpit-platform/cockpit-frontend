import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ExampleComponentComponent} from './example-component/example-component.component';
import {PluginConfig, PluginData, PluginPlacement} from '../plugin/plugin';
import {RouterModule} from '@angular/router';
import {PluginService} from '../plugin/plugin-service';
import {PLUGIN1_ROUTE} from './plugin1.route';
import {NavigationComponent} from './navigation/navigation.component';
import {StoreModule} from '@ngrx/store';
import {exampleReducer} from './state/reducers';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {CockpitSharedModule} from 'app/shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([PLUGIN1_ROUTE]),
    StoreModule.forFeature('plugin1Feature', {
      example: exampleReducer,
    }),
    MatDividerModule,
    MatListModule,
    MatIconModule,
    CockpitSharedModule
    ],
  exports: [RouterModule],
  declarations: [
    ExampleComponentComponent,
    NavigationComponent
  ],
  entryComponents: [
    ExampleComponentComponent,
    NavigationComponent
  ],
})
@PluginConfig({
  name: 'Plugin1',
  description:
    'Test cockpits plugin system. Reference implementation for plugins',
  placements: [
    new PluginPlacement({
      slot: 'dashboard',
      priority: 1,
      component: ExampleComponentComponent,
    }),
    new PluginPlacement({
      slot: 'navigation',
      priority: 1,
      component: NavigationComponent,
    })
  ],
})
export class Plugin1Module {

  constructor(private pluginService: PluginService) {
    this.pluginService.registerPlugin(this);
  }
}
