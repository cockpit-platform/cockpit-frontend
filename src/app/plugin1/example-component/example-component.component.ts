import { Component, OnInit } from '@angular/core';
import {Plugin1DataService} from '../state/plugin1-service';
import {ExampleModel} from '../state/models';

@Component({
  selector: 'app-example-component',
  templateUrl: './example-component.component.html'
})
export class ExampleComponentComponent implements OnInit {
  x = false;
  example: ExampleModel;
  constructor(private stateService: Plugin1DataService) {}
  ngOnInit() {
    this.stateService.loadState();
    this.stateService.example.subscribe(val => this.example = val);
  }
}
