import {Route} from '@angular/router';
import {AppContainerComponent} from 'app/app-container/app-container.component';
import {ExampleComponentComponent} from './example-component/example-component.component';
import {AuthGuard} from 'app/shared/profiles-data.service';

export const PLUGIN1_ROUTE: Route = {
  path: 'app',
  component: AppContainerComponent,
  canActivate: [AuthGuard],
  children: [ {
    path: 'plugin1',
    component: ExampleComponentComponent,
  },
  ]
};
