import { ExampleModel } from './models';
import * as ExampleActions from './example.actions';

export function exampleReducer(state: ExampleModel, action: ExampleActions.All): ExampleModel {
  switch (action.type) {
    case ExampleActions.ADD:
      return action.payload;
    case ExampleActions.UPDATE:
      return action.payload;

    default:
      return state;
  }
}



