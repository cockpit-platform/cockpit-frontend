import { Injectable } from '@angular/core';
import {
  createFeatureSelector,
  createSelector,
  select,
  Store,
} from '@ngrx/store';
import {Observable, ReplaySubject} from 'rxjs';
import { ExampleModel, Plugin1State } from './models';
import {Plugin1Module} from 'app/plugin1/plugin1.module';

const EXAMPLE: ExampleModel = {
  hallo: 'Hello',
  world: 'world',
};

@Injectable({
  providedIn: 'root'
})
export class Plugin1DataService {
  example: Observable<ExampleModel>;

  constructor(private store: Store<Plugin1State>) {
    const plugin1FeatureSelector = createFeatureSelector('plugin1Feature');
    const exampleSelector = createSelector(plugin1FeatureSelector, (state: Plugin1State) => state.example);
    this.example = store.select(exampleSelector);
  }

  loadState() {
    this.store.dispatch({ type: 'ADD', payload: EXAMPLE});
  }
}
