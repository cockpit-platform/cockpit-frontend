
import { Action } from '@ngrx/store';
import {ExampleModel} from './models';

export const ADD = 'ADD';
export const UPDATE = 'UPDATE';

export class Add implements Action {
  readonly type = ADD;

  constructor(public payload: ExampleModel) {}
}


export class Update implements Action {
  readonly type = UPDATE;

  constructor(public payload: ExampleModel) {}
}

export type All
  = Add
  | Update;
