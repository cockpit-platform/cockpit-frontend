import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {DashboardComponent} from 'app/dashboard/dashboard.component';
import {RouterModule} from '@angular/router';
import {DASHBOARD_ROUTES} from 'app/dashboard/dashboard.route';
import {CockpitPluginModule} from 'app/plugin/plugin.module';
import {CockpitSharedModule} from 'app/shared/shared.module';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    RouterModule.forChild(DASHBOARD_ROUTES),
    FlexLayoutModule,
    CockpitPluginModule,
    CockpitSharedModule
  ],
  exports: [DashboardComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CockpitDashboardModule {
  static forRoot() {
    return {
      ngModule: CockpitDashboardModule
    };
  }
}
