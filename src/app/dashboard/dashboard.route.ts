import {Routes} from '@angular/router';
import {DashboardComponent} from 'app/dashboard/dashboard.component';
import {UserListComponent} from 'app/user/user-list/user-list.component';
import {AppContainerComponent} from 'app/app-container/app-container.component';
import {AuthGuard} from 'app/shared/profiles-data.service';

export const DASHBOARD_ROUTES: Routes = [
  {
    path: 'app',
    component: AppContainerComponent,
    canActivate: [AuthGuard],
    children: [{
      path: 'dashboard',
      component: DashboardComponent
    }],
  },
  {
    path: '',
    redirectTo: '/app/dashboard',
    pathMatch: 'full'
  }
];
