import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import {
  Component,
  OnInit,
} from '@angular/core';
import {User, LegalOrga, Project, Collective} from 'app/shared/model/models';
import {ProfilesDataService} from 'app/shared/profiles-data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  user: User;
  users: User[];
  collectives: Collective[];
  legalOrgas: LegalOrga[];
  projects: Project[];
  subCollectives: Subscription;
  subLegalOrgas: Subscription;
  subProjects: Subscription;
  subUsers: Subscription;

  slugify = require('slugify');

  constructor(public profilesDataService: ProfilesDataService
  ) {
  }

  ngOnInit() {
    this.profilesDataService.auth.subscribe(auth => this.user = auth.user);

    this.subUsers = this.profilesDataService.users.subscribe(s => this.users = s);

    this.subCollectives = this.profilesDataService.collectives.subscribe(s => {
      this.collectives = s;
    });

    this.subLegalOrgas = this.profilesDataService.legalOrgas.subscribe(s => {
      this.legalOrgas = s;
    });

    this.subProjects = this.profilesDataService.projects.subscribe(s => {
      this.projects = s;
    });
  }

  getCollectives(): Collective[] {
    return this.collectives.filter(entry => entry.members.includes(this.user.url));
  }

  getLegalOrgas(): LegalOrga[] {
    return this.legalOrgas.filter(entry => entry.members.includes(this.user.url));
  }

  getProjects(): Project[] {
    return this.projects.filter(entry => entry.members.includes(this.user.url));
  }

  getActiveProjects(): Project[] {
    return this.projects.filter(entry => entry.members.includes(this.user.url) && !entry.deactivated_timestamp);
  }

  getDeactiveProjects(): Project[] {
    return this.projects.filter(entry => entry.members.includes(this.user.url) && entry.deactivated_timestamp);
  }

  getAverageMembersOfCollectives(): number {

    return (this.collectives.map(entry => entry.members.length)
                            .reduce((acc, cur) => acc + cur)) / this.collectives.length;
  }
}
