import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public isMenuCollapsed = true;
  public isOrgaCollapsed = true;
  public menuClasses = { 'small-screen-menu': false, 'nav-side-menu': true };

  constructor() {
  }

  ngOnInit() {
  }

  menuCollapse() {
    if (this.isMenuCollapsed) {
      this.menuClasses['small-screen-menu'] = true;
    } else {
      this.menuClasses['small-screen-menu'] = false;
    }
    this.isMenuCollapsed = !this.isMenuCollapsed;
  }

}
