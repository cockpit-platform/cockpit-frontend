import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import {User} from 'app/shared/model/models';
import {ProfilesDataService} from 'app/shared/profiles-data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user: User;
  @Output() toggleSidenav = new EventEmitter<void>();

  constructor(private profilesDataService: ProfilesDataService, private router: Router) {
  }

  ngOnInit() {
    this.profilesDataService.auth.subscribe(auth => this.user = auth.user);
  }

  logout() {
    this.profilesDataService.logout();
  }
}
