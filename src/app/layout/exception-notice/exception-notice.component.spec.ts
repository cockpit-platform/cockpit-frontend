import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceptionNoticeComponent } from './exception-notice.component';

describe('ExceptionNoticeComponent', () => {
  let component: ExceptionNoticeComponent;
  let fixture: ComponentFixture<ExceptionNoticeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceptionNoticeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceptionNoticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
