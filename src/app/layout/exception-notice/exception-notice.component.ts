import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import {ExceptionModel} from 'app/shared/model/models';
import {ProfilesDataService} from 'app/shared/profiles-data.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-exception-notice',
  templateUrl: './exception-notice.component.html',
  styleUrls: ['./exception-notice.component.scss']
})
export class ExceptionNoticeComponent implements OnInit, OnDestroy {
  isCollapsed = true;
  exception: ExceptionModel;
  subExc: Subscription;

  constructor(private profilesDataService: ProfilesDataService,
             private snackBar: MatSnackBar) {
  }

  close() {
    // this.isCollapsed = true;
    // this.profilesDataService.clearException();
  }

  ngOnInit() {
    this.subExc = this.profilesDataService.exception.subscribe(s => {
      this.exception = s;
      if (s) {
        this.isCollapsed = false;
        const snackBarRef = this.snackBar.open(this.exception.message, 'Schließen', {
          duration: 3000
        });

        snackBarRef.afterDismissed().subscribe(() => this.profilesDataService.clearException());
      }
    });
  }

  ngOnDestroy() {
    this.subExc.unsubscribe();
  }
}
