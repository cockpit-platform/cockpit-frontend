import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CockpitSharedModule} from 'app/shared/shared.module';
import {RouterModule} from '@angular/router';
import {LoginComponent} from 'app/login/login.component';
import {LOGIN_ROUTE} from 'app/login/login.route';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  imports: [CockpitSharedModule.forRoot(), RouterModule.forChild([LOGIN_ROUTE]), FlexLayoutModule, MatButtonModule, MatCheckboxModule,
    MatInputModule, MatFormFieldModule],
  declarations: [LoginComponent],
  exports: [LoginComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CockpitLoginModule {
  static forRoot() {
    return {
      ngModule: CockpitLoginModule
    };
  }
}
