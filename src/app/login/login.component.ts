import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, forkJoin } from 'rxjs';
import {ProfilesDataService} from 'app/shared/profiles-data.service';
import {User} from 'app/shared/model/models';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  // submitted = false;
  // onSubmit() { this.submitted = true; }
  // error: ExceptionModel;
  // subAuth: Subscription;

  constructor(private profilesDataService: ProfilesDataService, private router: Router) {
  }

  login(username, password) {
    const user: User = {
      username: username,
      password: password,
      email: 'null@null.de',
      unstructured_info: {},
      is_freelancer: false
    };
    // init data
    this.profilesDataService.authenticate(user).subscribe(res => {
      forkJoin(
        this.profilesDataService.loadUsers(),
        this.profilesDataService.loadCollectives(),
        this.profilesDataService.loadLegalOrgas(),
        this.profilesDataService.loadProjects()
      ).subscribe(res2 => {
        this.profilesDataService.refreshToken().subscribe(token => console.log(token));
        this.router.navigate(['app', 'dashboard']);
      });
    });
    // this.subAuth = this.profilesDataService.auth.subscribe(auth => {
    //   if (auth) {
    //     // const providers = ReflectiveInjector.resolve([Router, ProfilesDataService]);
    //     // onAppInit(this.injector)().then(() => {
    //     //   this.router.navigate(['/app/dashboard']);
    //     // });
    //     this.router.navigate(['/app/dashboard']);
    //     // subAuth.unsubscribe();
    //   }
    // });
  }

  ngOnInit() {
    // this.profilesDataService.exception.subscribe(exc => {this.error = exc; console.log('Exc: ' + exc); });
  }

  ngOnDestroy() {
    // this.subAuth.unsubscribe();
  }

}
