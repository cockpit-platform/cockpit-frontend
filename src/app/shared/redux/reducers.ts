import { Action, State } from '@ngrx/store';
import {AuthModel, User, ExceptionModel, Collective, LegalOrga, Project} from '../model/models';
import * as UserActions from './user.actions';
import * as AuthActions from './auth.actions';
import * as ExceptionActions from './exception.actions';
import * as CollectiveActions from './collective.actions';
import * as LegalOrgaActions from './legal-orga.actions';
import * as ProjectActions from './project.actions';

export function userReducer(state: User[] = [], action: UserActions.All): User[] {
  switch (action.type) {
    case UserActions.ADD:
      return action.payload;
    case UserActions.UPDATE:
      return state.map(user => {
        return user.url === action.payload.url ? Object.assign({}, user, action.payload) : user;
      });

    case UserActions.DELETE:
      return state.filter(user => {
        return user.url !== action.payload.url;
      });

    default:
      return state;
  }
}

export function collectiveReducer(state: Collective[] = [], action: CollectiveActions.All): Collective[] {
  switch (action.type) {
    case CollectiveActions.ADD:
      return action.payload;
    case CollectiveActions.APPEND:
      return state.concat(action.payload);
    case CollectiveActions.UPDATE:
      return state.map(orga => {
        return orga.url === action.payload.url ? Object.assign({}, orga, action.payload) : orga;
      });

    case CollectiveActions.DELETE:
      return state.filter(orga => {
        return orga.url !== action.payload.url;
      });

    default:
      return state;
  }
}

export function legalOrgaReducer(state: LegalOrga[] = [], action: LegalOrgaActions.All): LegalOrga[] {
  switch (action.type) {
    case LegalOrgaActions.ADD:
      return action.payload;
    case LegalOrgaActions.APPEND:
      return state.concat(action.payload);
    case LegalOrgaActions.UPDATE:
      return state.map(orga => {
        return orga.url === action.payload.url ? Object.assign({}, orga, action.payload) : orga;
      });

    case LegalOrgaActions.DELETE:
      return state.filter(orga => {
        return orga.url !== action.payload.url;
      });

    default:
      return state;
  }
}

export function projectReducer(state: Project[] = [], action: ProjectActions.All): Project[] {
  switch (action.type) {
    case ProjectActions.ADD:
      return action.payload;
    case ProjectActions.APPEND:
      return state.concat(action.payload);
    case ProjectActions.UPDATE:
      return state.map(orga => {
        return orga.url === action.payload.url ? Object.assign({}, orga, action.payload) : orga;
      });

    case ProjectActions.DELETE:
      return state.filter(orga => {
        return orga.url !== action.payload.url;
      });

    default:
      return state;
  }
}

export function authReducer(state: AuthModel, action: AuthActions.All): AuthModel {
  switch (action.type) {
    case AuthActions.AUTHENTICATE:
      return action.payload;
    // case AuthActions.LOGOUT:
    //   return null;
    case AuthActions.UPDATE:
      return {token: state.token, refresh: state.refresh, user: action.payload};

    default:
      return state;
  }
}

// unused at the moment a reload is prefered
// reset all state
export function logoutMetaReducer(reducer) {
  return function (state, action) {
    return reducer(action.type === AuthActions.LOGOUT ? undefined : state, action);
  };
}

export function exceptionReducer(state: ExceptionModel, action: ExceptionActions.All): ExceptionModel {
  switch (action.type) {
    case ExceptionActions.RAISE:
      return action.payload;
    case ExceptionActions.CLEAR:
      return null;

    default:
      return state;
  }
}
