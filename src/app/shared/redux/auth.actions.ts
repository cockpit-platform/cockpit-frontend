import { Action } from '@ngrx/store';
import {AuthModel, User} from '../model/models';

export const AUTHENTICATE = 'AUTHENTICATE';
export const LOGOUT = 'LOGOUT';
export const UPDATE = 'AUTH_UPDATE';

export class Authenticate implements Action {
  readonly type = AUTHENTICATE;

  constructor(public payload: AuthModel) {}
}

export class Logout implements Action {
  readonly type = LOGOUT;

  constructor() {}
}

export class Update implements Action {
  readonly type = UPDATE;

  constructor(public payload: User) {}
}

export type All
  = Authenticate
  | Logout
  | Update;
