import { Action } from '@ngrx/store';
import {User} from '../model/models';

export const ADD = 'USERS_ADD';
export const UPDATE = 'USERS_UPDATE';
export const DELETE = 'USERS_DELETE';

export class AddUsers implements Action {
  readonly type = ADD;

  constructor(public payload: User[]) {}
}

export class Delete implements Action {
  readonly type = DELETE;

  constructor(public payload: User) {}
}

export class Update implements Action {
  readonly type = UPDATE;

  constructor(public payload: User) {}
}

export type All
  = AddUsers
  | Update
  | Delete;
