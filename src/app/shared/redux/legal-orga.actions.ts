import { Action } from '@ngrx/store';
import {LegalOrga} from '../model/models';

export const ADD = 'LEGALORGA_ADD';
export const APPEND = 'LEGALORGA_APPEND';
export const UPDATE = 'LEGALORGA_UPDATE';
export const DELETE = 'LEGALORGA_DELETE';

export class Add implements Action {
  readonly type = ADD;

  constructor(public payload: LegalOrga[]) {}
}

export class Append implements Action {
  readonly type = APPEND;

  constructor(public payload: LegalOrga[]) {}
}

export class Delete implements Action {
  readonly type = DELETE;

  constructor(public payload: LegalOrga) {}
}

export class Update implements Action {
  readonly type = UPDATE;

  constructor(public payload: LegalOrga) {}
}

export type All
  = Add
  | Append
  | Delete
  | Update;
