import { Action } from '@ngrx/store';
import {Project} from '../model/models';

export const ADD = 'PROJECT_ADD';
export const APPEND = 'PROJECT_APPEND';
export const UPDATE = 'PROJECT_UPDATE';
export const DELETE = 'PROJECT_DELETE';

export class Add implements Action {
  readonly type = ADD;

  constructor(public payload: Project[]) {}
}

export class Append implements Action {
  readonly type = APPEND;

  constructor(public payload: Project[]) {}
}

export class Delete implements Action {
  readonly type = DELETE;

  constructor(public payload: Project) {}
}

export class Update implements Action {
  readonly type = UPDATE;

  constructor(public payload: Project) {}
}

export type All
  = Add
  | Append
  | Delete
  | Update;
