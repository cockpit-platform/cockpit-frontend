import { Action } from '@ngrx/store';
import {Collective} from '../model/models';

export const ADD = 'COLLECTIVE_ADD';
export const APPEND = 'COLLECTIVE_APPEND';
export const UPDATE = 'COLLECTIVE_UPDATE';
export const DELETE = 'COLLECTIVE_DELETE';

export class Add implements Action {
  readonly type = ADD;

  constructor(public payload: Collective[]) {}
}

export class Append implements Action {
  readonly type = APPEND;

  constructor(public payload: Collective[]) {}
}

export class Delete implements Action {
  readonly type = DELETE;

  constructor(public payload: Collective) {}
}

export class Update implements Action {
  readonly type = UPDATE;

  constructor(public payload: Collective) {}
}

export type All
  = Add
  | Append
  | Delete
  | Update;
