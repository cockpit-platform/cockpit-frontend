import { Action } from '@ngrx/store';
import {ExceptionModel} from '../model/models';

export const RAISE = 'EXC_RAISE';
export const CLEAR = 'EXC_CLEAR';

export class Raise implements Action {
  readonly type = RAISE;

  constructor(public payload: ExceptionModel) {}
}

export class Clear implements Action {
  readonly type = CLEAR;

  constructor() {}
}

export type All
  = Raise
  | Clear;

