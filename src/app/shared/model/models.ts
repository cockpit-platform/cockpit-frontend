interface UnstructuredUserInfo {
  short_intro?: string;
}

export interface User {
  url?: string;
  username: string;
  email: string;
  last_name?: string;
  first_name?: string;
  password?: string;
  street?: string;
  plz?: string;
  city?: string;
  phone_mobile?: string;
  phone_land?: string;
  phone_office?: string;
  jabber?: string;
  iban?: string;
  is_freelancer: boolean;
  tax_id?: string;
  unstructured_info: UnstructuredUserInfo;
  deleted_timestamp?: Date;
  is_active?: boolean;
}

export interface LegalOrga {
  url?: string;
  name: string;
  description?: string;
  email: string;
  members: string[];
  orga_admin: string;
  deleted_timestamp?: Date;
  legal_form: 'gbr' | 'gmbh' | 'ag' | 'kg' | 'gmbhcokg' | 'eg' | 'ev' | 'pg' | 'ohg' | 'ug' | 'st';
  tax_id: string;
  ust_id?: string;
  iban: string;
  street: string;
  plz: string;
  city: string;
  phone: string;
  fax?: string;
}

export interface Collective {
  url?: string;
  name: string;
  description?: string;
  email?: string;
  members: string[];
  orga_admin: string;
  deleted_timestamp?: Date;
  phone: string;
  fax?: string;
  street: string;
  legal_form: 'gbr' | 'gmbh' | 'ag' | 'kg' | 'gmbhcokg' | 'eg' | 'ev' | 'pg' | 'ohg' | 'ug' | 'st';
  iban: string;
}


export interface Project {
  url?: string;
  name: string;
  description?: string;
  email?: string;
  members: string[];
  orga_admin: string;
  deleted_timestamp?: Date;
  deactivated_timestamp?: string;
}

export type Orga = LegalOrga | Collective | Project;

export interface AuthModel {
  user: User;
  token: string;
  refresh: string;
}

export interface ExceptionModel {
  message: string;
  type: string;
}
