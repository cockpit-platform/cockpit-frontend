import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {StoreModule} from '@ngrx/store';
import {CommonModule} from '@angular/common';
import {
  authReducer,
  collectiveReducer,
  exceptionReducer,
  legalOrgaReducer,
  projectReducer,
  userReducer
} from 'app/shared/redux/reducers';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, HttpClientModule, StoreModule.forRoot({
    users: userReducer,
    collectives: collectiveReducer,
    legalOrgas: legalOrgaReducer,
    projects: projectReducer,
    auth: authReducer,
    exception: exceptionReducer
  })],
  exports: [CommonModule, FormsModule, ReactiveFormsModule, HttpClientModule, StoreModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CockpitSharedLibsModule {
  static forRoot() {
    return {
      ngModule: CockpitSharedLibsModule
    };
  }
}
