import { TestBed, inject } from '@angular/core/testing';

import { ProfilesDataService } from './profiles-data.service';

describe('ProfilesDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfilesDataService]
    });
  });

  it('should be created', inject([ProfilesDataService], (service: ProfilesDataService) => {
    expect(service).toBeTruthy();
  }));
});
