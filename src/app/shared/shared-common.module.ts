import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {ProfilesDataService} from 'app/shared/profiles-data.service';
import {CockpitStdFormControlModule} from 'app/shared/std-form-control/std-form-control.module';
import {OrgaEntryComponent} from 'app/shared/orga-entry/orga-entry.component';
import {RouterModule} from '@angular/router';
import {CockpitSharedLibsModule} from 'app/shared/shared-libs.module';
import {MatCardModule} from '@angular/material/card';
import {FlexLayoutModule} from '@angular/flex-layout';
import {OrgaFilteredListComponent} from './orga-filtered-list/orga-filtered-list.component';
import {DashboardTileComponent} from './dashboard-tile/dashboard-tile.component';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  imports: [
    CockpitSharedLibsModule,
    CockpitStdFormControlModule,
    RouterModule,
    FlexLayoutModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
  ],
  declarations: [OrgaEntryComponent, OrgaFilteredListComponent, DashboardTileComponent],
  providers: [ProfilesDataService],
  exports: [CockpitStdFormControlModule, OrgaEntryComponent, OrgaFilteredListComponent, DashboardTileComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CockpitSharedCommonModule {
  static forRoot() {
    return {
      ngModule: CockpitSharedCommonModule
    };
  }
}
