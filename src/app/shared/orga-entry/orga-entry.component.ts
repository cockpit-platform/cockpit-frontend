import { Component, OnInit, Input } from '@angular/core';
import {User, Orga} from 'app/shared/model/models';

@Component({
  selector: 'app-orga-entry',
  templateUrl: './orga-entry.component.html',
  styleUrls: ['./orga-entry.component.scss']
})
export class OrgaEntryComponent implements OnInit {
  // FIXME: Bad quickfix, we need to distinguish between orga types in template somehow
  @Input()
  organisation: any;

  @Input()
  admin: User;

  @Input()
  members: User[];

  @Input()
  type: string;

  slugify = require('slugify');

  constructor() {
  }

  ngOnInit() {
  }
}
