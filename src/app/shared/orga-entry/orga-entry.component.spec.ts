import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgaEntryComponent } from './orga-entry.component';

describe('OrgaEntryComponent', () => {
  let component: OrgaEntryComponent;
  let fixture: ComponentFixture<OrgaEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgaEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgaEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
