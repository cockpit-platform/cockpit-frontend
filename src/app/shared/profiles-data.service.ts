import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Store } from '@ngrx/store';
import { User, AuthModel, ExceptionModel, Collective, LegalOrga, Project } from './model/models';
import {interval, Observable, ReplaySubject} from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';


export interface ProfilesState {
  auth: AuthModel;
  users: User[];
  collectives: Collective[];
  legalOrgas: LegalOrga[];
  projects: Project[];
  exception: ExceptionModel;
}

const HEADERS = new HttpHeaders({ 'Content-Type': 'application/json' });
const AUTH_API_URL = environment['serverBaseUrl'] + 'api-jwt-auth/token/';
const REFRESH_TOKEN_API_URL = environment['serverBaseUrl'] + 'api-jwt-auth/refresh/';
const USER_API_URL = environment['serverBaseUrl'] + 'profiles/users/';
const COLLECTIVE_API_URL = environment['serverBaseUrl'] + 'profiles/collectives/';
const LEGALORGA_API_URL = environment['serverBaseUrl'] + 'profiles/legal_orgas/';
const PROJECT_API_URL = environment['serverBaseUrl'] + 'profiles/projects/';

@Injectable()
export class ProfilesDataService {
  users: Observable<User[]>;
  auth: Observable<AuthModel>;
  collectives: Observable<Collective[]>;
  legalOrgas: Observable<LegalOrga[]>;
  projects: Observable<Project[]>;
  exception: Observable<ExceptionModel>;

  constructor(private http: HttpClient, private store: Store<ProfilesState>, private router: Router, private jwtHelper: JwtHelperService) {
    this.auth = store.select('auth');
    this.users = store.select('users');
    this.collectives = store.select('collectives');
    this.legalOrgas = store.select('legalOrgas');
    this.projects = store.select('projects');
    this.exception = store.select('exception');
  }

  private _httpErrorHandler(err: HttpErrorResponse): void {
    if (err.error instanceof Error) {
      this.exception.subscribe(s => console.log(s.message));
      // A client-side or network error occurred. Handle it accordingly.
      this.raiseException(`Verbindungsfehler: ${err.error.message}`, 'http_error');
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      // relogin if we have a password, but the token expired
      if (err.status === 401) {
        this.raiseException(`Serverfehler ${err.status}, Antwort: ${err.error}`, 'http_error');
        let hasCredentials = false;
        let user: User;
        this.auth.subscribe(auth => {
          if (auth != null && auth.user) {
            user = auth.user;
            hasCredentials = !!auth.user.password;
          }
        });
        if (hasCredentials) {
          this.authenticate(user);
        } else {
          this.router.navigate(['/login']);
        }
        // this.clearException();
      } else {
        this.raiseException(`Serverfehler ${err.status}, Antwort: ${err.error}`, 'http_error');
      }
    }
  }

  authenticate(user: User): Observable<object> {
    let token: string;
    let refresh: string;
    const body = new HttpParams()
      .set('username', user.username)
      .set('password', user.password);

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

    const postObservable = this.http.post(AUTH_API_URL, body.toString(), { headers });

    const subject = new ReplaySubject<object>(1);
    postObservable.subscribe(payload => {
      token = payload['access'];
      refresh = payload['refresh'];
      this.setAccessToken(token);
      this.setRefreshToken(refresh);
      this.http.get<any[]>(USER_API_URL).subscribe(res => {
        this.store.dispatch({ type: 'USERS_ADD', payload: res });
        const auth_user: User = res.find(currentUser => user.username === currentUser.username);
        auth_user.password = user.password;
        const payload2: AuthModel = { user: auth_user, token: token, refresh: refresh };
        this.store.dispatch({ type: 'AUTHENTICATE', payload: payload2 });
      });
    },
      (err: HttpErrorResponse) => {
        this._httpErrorHandler(err);
        this.router.navigate(['/login']);
      }
    );

    //    postObservable.subscribe(subject);
    return postObservable;
  }

  refreshAuth() {
    // used on page reload if we have token from localstorage, but no user
    // no http here as jwt-refresh does its job
    const token = localStorage.getItem('token');
    const refresh = localStorage.getItem('refresh');

    if (token) {
      if (this.jwtHelper.isTokenExpired(token)) {
        this.setAccessToken(null);
        return;
      }
      let user: User;
      const subUser = this.users.subscribe(entry => {
        user = entry.find(entry2 => entry2.username === this.jwtHelper.decodeToken(token)['username']);
        if (user) {
          const authModel: AuthModel = { user: user, token: token, refresh: refresh };
          this.store.dispatch({ type: 'AUTHENTICATE', payload: authModel });
        }
      });
    }
  }

  refreshToken(): Observable<string> {
    // background worker to regularly renew JWT tokens
    const self = this;
    return Observable.create(function (observer) {
      const interval = setInterval(() => {
        const refresh = localStorage.getItem('refresh');
        const token = localStorage.getItem('token');
        if (token) {
          const tokenTTL = +self.jwtHelper.getTokenExpirationDate(token) - Date.now();
          if (tokenTTL < 30000) {
            const responseObservable = self.http.post(REFRESH_TOKEN_API_URL, {'refresh': refresh});
            responseObservable.subscribe(response => {
              self.setAccessToken(response['access']);
              observer.next(response['access']);
            });
          }

        }
      }, 1000);

      return () => clearInterval(interval);
    });
  }

  private setAccessToken(accessToken: string) {
    if (!accessToken) {
      localStorage.removeItem('token');
    } else {
      localStorage.setItem('token', accessToken);
    }
  }

  private setRefreshToken(refreshToken: string) {
    if (!refreshToken) {
      localStorage.removeItem('refresh');
    } else {
      localStorage.setItem('refresh', refreshToken);
    }
  }

  logout() {
    localStorage.clear();
    window.location.reload();
  }

  loadUsers(): Observable<object> {
    const obs = this.http.get(USER_API_URL);
    obs.subscribe(res => {
      this.store.dispatch({ type: 'USERS_ADD', payload: res });
    },
      (err: HttpErrorResponse) => {
        this._httpErrorHandler(err);
      });
    return obs;
  }

  loadUserDetails(url: string): Observable<object> {
    const obs = this.http.get(url, { headers: HEADERS });
    obs.subscribe(res => {
      this.store.dispatch({ type: 'USERS_UPDATE', payload: res });
    },
      (err: HttpErrorResponse) => {
        this._httpErrorHandler(err);
      });
    return obs;
  }

  loadCollectives(): Observable<object> {
    const obs = this.http.get(COLLECTIVE_API_URL, { headers: HEADERS });
    obs.subscribe(res => {
      this.store.dispatch({ type: 'COLLECTIVE_ADD', payload: res });
    },
      (err: HttpErrorResponse) => {
        this._httpErrorHandler(err);
      });
    return obs;
  }

  loadLegalOrgas(): Observable<object> {
    const obs = this.http.get(LEGALORGA_API_URL, { headers: HEADERS });
    obs.subscribe(res => {
      this.store.dispatch({ type: 'LEGALORGA_ADD', payload: res });
    },
      (err: HttpErrorResponse) => {
        this._httpErrorHandler(err);
      });
    return obs;
  }

  loadProjects(): Observable<object> {
    const obs = this.http.get(PROJECT_API_URL, { headers: HEADERS });
    obs.subscribe(res => {
      this.store.dispatch({ type: 'PROJECT_ADD', payload: res });
    },
      (err: HttpErrorResponse) => {
        this._httpErrorHandler(err);
      });
    return obs;
  }


  raiseException(message: string, type: string) {
    this.store.dispatch({ type: 'EXC_RAISE', payload: { type: type, message: message } });
  }

  clearException() {
    this.store.dispatch({ type: 'EXC_CLEAR' });
  }


  updateUserDetails(user: User) {
    this.http.patch(user.url, user, { headers: HEADERS }).subscribe(res => {
      // copy pw, if exists
      // if (user.password) {
      //   res['password'] = user.password;
      // }
      // res['password'] = user.password ? user.password : undefined;
      this.store.dispatch({ type: 'USERS_UPDATE', payload: res });
    },
      (err: HttpErrorResponse) => {
        this._httpErrorHandler(err);
        // timeout to overwrite std http_error message
        setTimeout(() => {
          if (err['_body']) {
            const djangoError = JSON.parse(err['_body']);
            let msg = '';
            Object.keys(djangoError).forEach((key) => {
              msg = msg + `Feld ${key}: ${djangoError[key]}\n`;
            });
            this.raiseException(msg, 'error');
          }
        }, 300);
      });
  }

  createCollective(collective: Collective) {
    this.http.post(COLLECTIVE_API_URL, collective, { headers: HEADERS }).subscribe(res => {
      this.store.dispatch({ type: 'COLLECTIVE_APPEND', payload: collective });
    },
      (err: HttpErrorResponse) => {
        this._httpErrorHandler(err);
        // timeout to overwrite std http_error message
        setTimeout(() => {
          if (err['_body']) {
            const djangoError = JSON.parse(err['_body']);
            let msg = '';
            Object.keys(djangoError).forEach((key) => {
              msg = msg + `Feld ${key}: ${djangoError[key]}\n`;
            });
            this.raiseException(msg, 'error');
          }
        }, 300);
      });
  }

  updateCollective(collective: Collective) {
    this.http.patch(collective.url, collective, { headers: HEADERS }).subscribe(res => {
      this.store.dispatch({ type: 'COLLECTIVE_UPDATE', payload: collective });
    },
      (err: HttpErrorResponse) => {
        this._httpErrorHandler(err);
        // timeout to overwrite std http_error message
        setTimeout(() => {
          if (err['_body']) {
            const djangoError = JSON.parse(err['_body']);
            let msg = '';
            Object.keys(djangoError).forEach((key) => {
              msg = msg + `Feld ${key}: ${djangoError[key]}\n`;
            });
            this.raiseException(msg, 'error');
          }
        }, 300);
      });
  }

  deleteCollective(collective: Collective) {
    this.http.delete(collective.url, { headers: HEADERS }).subscribe(res => {
      this.store.dispatch({ type: 'COLLECTIVE_DELETE', payload: collective });
    }, (err: HttpErrorResponse) => {
      this.raiseException(`Kollektiv ${collective.name} konnte nicht gelöscht werden.`, 'error');
    });
  }

  createLegalOrga(legalOrga: LegalOrga) {
    this.http.post(LEGALORGA_API_URL, legalOrga, { headers: HEADERS }).subscribe(res => {
      this.store.dispatch({ type: 'LEGALORGA_APPEND', payload: legalOrga });
    },
      (err: HttpErrorResponse) => {
        this._httpErrorHandler(err);
        // timeout to overwrite std http_error message
        setTimeout(() => {
          if (err['_body']) {
            const djangoError = JSON.parse(err['_body']);
            let msg = '';
            Object.keys(djangoError).forEach((key) => {
              msg = msg + `Feld ${key}: ${djangoError[key]}\n`;
            });
            this.raiseException(msg, 'error');
          }
        }, 300);
      });
  }

  updateLegalOrga(legalOrga: LegalOrga) {
    this.http.patch(legalOrga.url, legalOrga, { headers: HEADERS }).subscribe(res => {
      this.store.dispatch({ type: 'LEGALORGA_UPDATE', payload: legalOrga });
    },
      (err: HttpErrorResponse) => {
        this._httpErrorHandler(err);
        // timeout to overwrite std http_error message
        setTimeout(() => {
          if (err['_body']) {
            const djangoError = JSON.parse(err['_body']);
            let msg = '';
            Object.keys(djangoError).forEach((key) => {
              msg = msg + `Feld ${key}: ${djangoError[key]}\n`;
            });
            this.raiseException(msg, 'error');
          }
        }, 300);
      });
  }

  deleteLegalOrga(legalOrga: LegalOrga) {
    this.http.delete(legalOrga.url, { headers: HEADERS }).subscribe(res => {
      this.store.dispatch({ type: 'LEGALORGA_DELETE', payload: legalOrga });
    }, (err: HttpErrorResponse) => {
      this.raiseException(`Rechtsform ${legalOrga.name} konnte nicht gelöscht werden.`, 'error');
    });
  }

  createProject(project: Project) {
    this.http.post(PROJECT_API_URL, project, { headers: HEADERS }).subscribe(res => {
      this.store.dispatch({ type: 'PROJECT_APPEND', payload: project });
    },
      (err: HttpErrorResponse) => {
        this._httpErrorHandler(err);
        // timeout to overwrite std http_error message
        setTimeout(() => {
          if (err['_body']) {
            const djangoError = JSON.parse(err['_body']);
            let msg = '';
            Object.keys(djangoError).forEach((key) => {
              msg = msg + `Feld ${key}: ${djangoError[key]}\n`;
            });
            this.raiseException(msg, 'error');
          }
        }, 300);
      });
  }

  updateProject(project: Project) {
    this.http.patch(project.url, project, { headers: HEADERS }).subscribe(res => {
      this.store.dispatch({ type: 'PROJECT_UPDATE', payload: project });
    },
      (err: HttpErrorResponse) => {
        this._httpErrorHandler(err);
        // timeout to overwrite std http_error message
        setTimeout(() => {
          if (err['_body']) {
            const djangoError = JSON.parse(err['_body']);
            let msg = '';
            Object.keys(djangoError).forEach((key) => {
              msg = msg + `Feld ${key}: ${djangoError[key]}\n`;
            });
            this.raiseException(msg, 'error');
          }
        }, 300);
      });
  }

  deleteProject(project: Project) {
    this.http.delete(project.url, { headers: HEADERS }).subscribe(res => {
      this.store.dispatch({ type: 'PROJECT_DELETE', payload: project });
    }, (err: HttpErrorResponse) => {
      this.raiseException(`Projekt ${project.name} konnte nicht gelöscht werden.`, 'error');
    });
  }

}

// @Injectable()
// export class AuthGuard implements CanActivate {
//   constructor (private profilesDataService: ProfilesDataService, private router: Router) {}
//   canActivate (
//     route: ActivatedRouteSnapshot,
//     state: RouterStateSnapshot
//   ): Observable<boolean> | boolean {
//       let check: boolean;
//       this.profilesDataService.auth.map(authModel => authModel != null ).subscribe(check_out => check = check_out);
//     if (!check) {
//       this.router.navigate(['/login']);
//     } else {
//       return true;
//     }
//   }
// }

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private jwtHelper: JwtHelperService) { }

  canActivate() {
    if (!this.jwtHelper.isTokenExpired()) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
