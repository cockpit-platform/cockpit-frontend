import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CockpitSharedLibsModule} from 'app/shared/shared-libs.module';
import {CockpitSharedCommonModule} from 'app/shared/shared-common.module';

@NgModule({
  imports: [CockpitSharedLibsModule, CockpitSharedCommonModule],
  exports: [CockpitSharedLibsModule, CockpitSharedCommonModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CockpitSharedModule {
  static forRoot() {
    return {
      ngModule: CockpitSharedModule
    };
  }
}
