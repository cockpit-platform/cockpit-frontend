import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StdFormControlComponent } from './std-form-control.component';

describe('StdFormControlComponent', () => {
  let component: StdFormControlComponent;
  let fixture: ComponentFixture<StdFormControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StdFormControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StdFormControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
