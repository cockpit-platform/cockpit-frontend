import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {StdFormControlComponent} from 'app/shared/std-form-control/std-form-control.component';
import {CockpitSharedLibsModule} from 'app/shared/shared-libs.module';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';

@NgModule({
  imports: [
    CockpitSharedLibsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatCheckboxModule,
    MatSelectModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    FlexLayoutModule],
  declarations: [StdFormControlComponent],
  exports: [StdFormControlComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CockpitStdFormControlModule {
  static forRoot() {
    return {
      ngModule: CockpitStdFormControlModule
    };
  }
}
