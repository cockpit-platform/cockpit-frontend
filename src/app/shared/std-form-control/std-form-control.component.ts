import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatAutocomplete} from '@angular/material/autocomplete';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Observable} from 'rxjs';
import {startWith, map} from 'rxjs/operators';
import {MatChipInputEvent} from '@angular/material/chips';

export interface TagInputModel {
  value: string;
  display: string;
}

@Component({
  selector: 'lib-std-form-control',
  templateUrl: './std-form-control.component.html',
  styleUrls: ['./std-form-control.component.scss']
})
export class StdFormControlComponent implements OnInit {
  dbConfirmMessage: boolean;

  @Input()
  editMode: Object;

  formGroupValue: FormGroup;

  @Input()
  type: string;

  @Input()
  control: string;

  @Input()
  label?: string;

  @Input()
  dropDownOptions?: string[];

  @Input()
  tiNgModel?: TagInputModel[];

  @Input()
  tiAutocompleteItems?: TagInputModel[];

  tiFilteredAutocompleteItems: Observable<TagInputModel[]>;

  @Input()
  tiMaxItems?: number;

  @Output()
  tiNgModelChangeEvent = new EventEmitter<TagInputModel[]>();

  separatorKeysCodes: number[] = [ENTER, COMMA];

  tiCtrl = new FormControl();

  @ViewChild('tagInput', {static: false}) tagInput: ElementRef<HTMLInputElement>;

  @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;

  @Output()
  saveEvent = new EventEmitter<string>();

  @Output()
  cancelEvent = new EventEmitter<string>();

  @Output()
  formChange = new EventEmitter<FormGroup>();

  constructor() {

  }

  @Input()
  get formGroup() {
    return this.formGroupValue;
  }

  set formGroup(val) {
    this.formGroupValue = val;
    this.formChange.emit(this.formGroupValue);
  }

  save() {
    // console.log(this.control);
    this.saveEvent.emit(this.control);
  }

  cancel() {
    this.cancelEvent.emit(this.control);
  }

  tiSelected(event: MatAutocompleteSelectedEvent): void {
      this.tiNgModel.push({ value: event.option.value, display: event.option.viewValue});
      this.tiNgModelChangeEvent.emit(this.tiNgModel);
      this.tagInput.nativeElement.value = '';
      this.tiCtrl.setValue(null);
  }

  tiRemove(tag: TagInputModel): void {
    const index = this.tiNgModel.indexOf(tag);

    if (index >= 0) {
      this.tiNgModel.splice(index, 1);
      this.tiNgModelChangeEvent.emit(this.tiNgModel);
    }
  }

  private _filterTag(value: string): TagInputModel[] {
    const filterValue = value.toLowerCase();
    return this.tiAutocompleteItems.filter(tag => tag.display.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit() {
    if (this.type === 'tag-input' ) {
      this.tiFilteredAutocompleteItems = this.tiCtrl.valueChanges.pipe(
        startWith(null),
        map((tag: string | null) => tag ? this._filterTag(tag) : this.tiAutocompleteItems.slice()));
    }
  }

}
