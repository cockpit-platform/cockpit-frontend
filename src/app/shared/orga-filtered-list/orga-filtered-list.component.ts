import { Component, OnInit, Input, OnDestroy, SimpleChanges, OnChanges } from '@angular/core';
import {User, Orga} from 'app/shared/model/models';
import {Subscription, Observable, Subject, merge, BehaviorSubject} from 'rxjs';
import {startWith, map, distinctUntilChanged, debounceTime} from 'rxjs/operators';
import {ProfilesDataService} from 'app/shared/profiles-data.service';

@Component({
  selector: 'app-orga-filtered-list',
  templateUrl: './orga-filtered-list.component.html',
  styleUrls: ['./orga-filtered-list.component.scss']
})
export class OrgaFilteredListComponent implements OnInit, OnDestroy, OnChanges {

  @Input() type: string;
  @Input() orgas: Orga[];
  orgas$: Observable<Orga[]>;
  users: User[];
  subUsers: Subscription;
  searchSubject = new BehaviorSubject<string>('');
  orgasSubject = new Subject<Orga[]>();

  constructor(private profilesDataService: ProfilesDataService) { }

  ngOnInit() {
    this.subUsers = this.profilesDataService.users.subscribe(users => this.users = users);
    this.orgas$ = merge(
      this.orgasSubject.pipe(
        map(val => this.searchSubject.getValue().length < 2 ? val
          : this.filterOrgas(val, this.searchSubject.getValue())),
        startWith(this.orgas)
    ),
      this.searchSubject.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        map(term => term.length < 2 ? this.orgas
          : this.filterOrgas(this.orgas, term)),
      ));
  }

  ngOnDestroy() {
    this.subUsers.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['orgas']) {
      this.orgas = changes['orgas'].currentValue;
      this.orgasSubject.next(this.orgas);
    }
  }

  filterOrgas(orgas: Orga[], term: string) {
    return orgas.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10);
  }

  getAdmin(orga: Orga): User {
    return this.users.find(entry => entry.url === orga.orga_admin);
  }

  getMembers(orga: Orga): User[] {
    return this.users.filter(entry => orga.members.includes(entry.url));
  }

  searchChanged(search: string) {
    this.searchSubject.next(search);
  }

}
