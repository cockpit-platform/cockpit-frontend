import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgaFilteredListComponent } from './orga-filtered-list.component';

describe('OrgaFilteredListComponent', () => {
  let component: OrgaFilteredListComponent;
  let fixture: ComponentFixture<OrgaFilteredListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgaFilteredListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgaFilteredListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
