import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './layout/page-not-found/page-not-found.component';
import {DashboardComponent} from 'app/dashboard/dashboard.component';
import {AppContainerComponent} from 'app/app-container/app-container.component';
import {ProfileComponent} from 'app/user/profile/profile.component';
import {UserContainerComponent} from 'app/user/user-container/user-container.component';
import {LoginComponent} from 'app/login/login.component';
// import {AuthModule} from './auth.module';

const routes: Routes = [
  {
    path: 'login', component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    {enableTracing: true} // <-- debugging purposes only
  )],
  declarations: [PageNotFoundComponent],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
