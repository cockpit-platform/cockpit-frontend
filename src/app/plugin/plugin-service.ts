import {Injectable, NgModuleFactory, NgModuleRef, Injector, ComponentFactoryResolver, ViewContainerRef} from '@angular/core';
import {PluginData} from './plugin';
import {config, ReplaySubject} from 'rxjs';
import {Route, Router} from '@angular/router';


@Injectable({
providedIn: 'root'
})
export class PluginService {
  plugins: object[];
  change: ReplaySubject<any>;
  slugify = require('slugify');
  pluginModules: object[];

  //  constructor(private compiler: Compiler) {
  constructor(private router: Router, private injector: Injector) {
    this.plugins = [];
    this.pluginModules = [];
    // Change observable if the list of active plugin changes
    this.change = new ReplaySubject(1);
  }

  registerPlugin(ngModule: any) {
    this.pluginModules.push(ngModule);
  }


  loadPlugins() {
    for (const pluginModule of this.pluginModules) {
      this.loadPlugin(pluginModule);
    }
  }


    // Loads one individual plugin, instantiates it and appends it to the available list of plugins
    // (I couldn't fix typeing, because the plugin decorator appends an attribute in runtime -> any type)
  loadPlugin(pluginModule: object) {
        const pluginData = {
          name: Object.getPrototypeOf(pluginModule).constructor._pluginConfig['name'],
          type: pluginModule,
          // Reading the meta data previously stored by the @Plugin decorator
          config: Object.getPrototypeOf(pluginModule).constructor._pluginConfig,
          // instance: moduleRef
        };

        this.plugins = this.plugins.concat([pluginData]);
        // Add routes
        // app sub route
        // let subRoutes: Route[] = [];
        // this.router.config.forEach(route => {
        //   if (route.path === 'app' && route.children) {
        //     subRoutes = route.children;
        //   }
        // });
        // const nameSlug: string = this.slugify(pluginData.config.name);
        // subRoutes.push({
        //   path: nameSlug,
        //   loadChildren: () => pluginModulePromise
        // });
        // console.log(pluginData.config.name + ' route endpoint: ' + nameSlug);
        this.change.next(this.plugins);
    }



  // Get all plugin data objects for a given slot name
  getPluginData(slot) {
    return this.plugins.reduce((components: Object[], pluginData: any) => {
      return components.concat(
        pluginData.config.placements
          .filter((placement) => placement.slot === slot)
          .map((placement) => new PluginData(pluginData, placement))
      );
    }, []);
  }
}

