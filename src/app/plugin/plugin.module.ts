import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {PluginSlotDirective} from 'app/plugin/plugin-slot';

@NgModule({
  declarations: [PluginSlotDirective],
  imports: [
  ],
  exports: [PluginSlotDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CockpitPluginModule {
  static forRoot() {
    return {
      ngModule: CockpitPluginModule
    };
  }
}
