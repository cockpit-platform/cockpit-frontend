import {
  Directive,
  Input,
  Inject,
  ViewContainerRef,
  ComponentFactoryResolver,
  ValueProvider,
  OnChanges,
  OnDestroy,
  Injector,
} from '@angular/core';
import { PluginData } from './plugin';
import { PluginService } from './plugin-service';
import { Subscription } from 'rxjs';

@Directive({
  selector: 'app-plugin-slot',
})
export class PluginSlotDirective implements OnChanges, OnDestroy {
  @Input() name;
  viewContainerRef: ViewContainerRef;
  componentFactoryResolver: ComponentFactoryResolver;
  pluginService: PluginService;
  componentRefs: any[];
  pluginChangeSubscription: Subscription;

  constructor(
    @Inject(ViewContainerRef) viewContainerRef,
    @Inject(ComponentFactoryResolver) componentFactoryResolver,
    @Inject(PluginService) pluginService
  ) {
    this.viewContainerRef = viewContainerRef;
    this.componentFactoryResolver = componentFactoryResolver;
    this.pluginService = pluginService;
    this.componentRefs = [];
    // Subscribing to changes on the plugin service and re-initialize slot if needed
    this.pluginChangeSubscription = this.pluginService.change.subscribe(() =>
      this.initialize()
    );
  }

  initialize() {
    // If we don't have a valid name input, we shall return
    if (!this.name) {
      return;
    }

    // If we have already references to components within the plugin slot, we dispose them and clear the list
    if (this.componentRefs.length > 0) {
      this.componentRefs.forEach(componentRef => componentRef.destroy());
      this.componentRefs = [];
    }

    // Using the PluginService we can obtain all placement information relevant to this slot
    const pluginData: any = this.pluginService.getPluginData(this.name);
    // Using the placement priority to sort plugin components relevant to this slot
    pluginData.sort((a, b) =>
      a.placement.priority < b.placement.priority
        ? 1
        : a.placement.priority > b.placement.priority
        ? -1
        : 0
    );

    // Instantiating all plugin components relevant to this slot
    return Promise.all(
      pluginData.map(sPluginData =>
        this.instantiatePluginComponent(sPluginData)
      )
    );
  }

  // Method to instantiate a single component based on plugin placement information
  instantiatePluginComponent(pluginData) {
    // return this.componentResolver
    //   .resolveComponent(pluginData.placement.component)
    //   .then((componentFactory) => {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      pluginData.placement.component
    );
    // Get the injector of the plugin slot parent component
    // (found no info on deprecation replacement. warning maybe wrong)
    const contextInjector: Injector = this.viewContainerRef.injector;
    // Preparing additional PluginData provider for the created plugin component
    const providers: ValueProvider[] = [
      { provide: PluginData, useValue: pluginData },
    ];
    // We're creating a new child injector and provide the PluginData provider
    const childInjector = Injector.create({
      providers,
      parent: contextInjector,
    });
    // Now we can create a new component using the plugin slot view container and the resolved component factory
    const componentRef = this.viewContainerRef.createComponent(
      componentFactory,
      this.viewContainerRef.length,
      childInjector
    );
    // this.componentRefs.push(componentRef);
    // Perform change detection after component creation for OnPush and Detached parent views
    componentRef.changeDetectorRef.markForCheck();
    componentRef.changeDetectorRef.detectChanges();
    return componentRef;
  }

  // If the name input changes, we need to re-initialize the plugin components
  ngOnChanges() {
    this.initialize();
  }

  ngOnDestroy() {
    this.pluginChangeSubscription.unsubscribe();
  }
}
