import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalOrgaListComponent } from './legal-orga-list.component';

describe('LegalOrgaListComponent', () => {
  let component: LegalOrgaListComponent;
  let fixture: ComponentFixture<LegalOrgaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegalOrgaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalOrgaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
