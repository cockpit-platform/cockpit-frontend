import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import {ProfilesDataService} from 'app/shared/profiles-data.service';
import {LegalOrga} from 'app/shared/model/models';

@Component({
  selector: 'app-legal-orga-list',
  templateUrl: './legal-orga-list.component.html',
  styleUrls: ['./legal-orga-list.component.scss']
})
export class LegalOrgaListComponent implements OnInit, OnDestroy {
  legalOrgas: LegalOrga[];
  subLegalOrgas: Subscription;

  constructor(private profilesDataService: ProfilesDataService) { }

  ngOnInit() {
    this.subLegalOrgas = this.profilesDataService.legalOrgas.subscribe(legalOrgas => this.legalOrgas = legalOrgas);
  }

  ngOnDestroy() {
    this.subLegalOrgas.unsubscribe();
  }

}
