import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {LegalOrgaDetailComponent} from 'app/legal-orga/legal-orga-detail/legal-orga-detail.component';
import {LegalOrgaListComponent} from 'app/legal-orga/legal-orga-list/legal-orga-list.component';
import {CockpitSharedModule} from 'app/shared/shared.module';
import {RouterModule} from '@angular/router';
import {LEGAL_ORGA_ROUTE} from 'app/legal-orga/legal-orga.route';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  imports: [CockpitSharedModule.forRoot(), RouterModule.forChild([LEGAL_ORGA_ROUTE]), FlexLayoutModule, MatButtonModule, MatIconModule],
  declarations: [LegalOrgaDetailComponent, LegalOrgaListComponent],
  exports: [LegalOrgaDetailComponent, LegalOrgaListComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CockpitLegalOrgaModule {
  static forRoot() {
    return {
      ngModule: CockpitLegalOrgaModule
    };
  }
}
