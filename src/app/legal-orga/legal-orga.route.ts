import {Route} from '@angular/router';
import {LegalOrgaListComponent} from 'app/legal-orga/legal-orga-list/legal-orga-list.component';
import {LegalOrgaDetailComponent} from 'app/legal-orga/legal-orga-detail/legal-orga-detail.component';
import {AppContainerComponent} from 'app/app-container/app-container.component';
import {AuthGuard} from 'app/shared/profiles-data.service';

export const LEGAL_ORGA_ROUTE: Route = {
  path: 'app',
  component: AppContainerComponent,
  canActivate: [AuthGuard],
  children: [ {
    path: 'legal-orgas',
    component: LegalOrgaListComponent,
  },
  {
    path: 'legal-orga/:name',
    component: LegalOrgaDetailComponent
  }]
};
