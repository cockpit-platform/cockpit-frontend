import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalOrgaDetailComponent } from './legal-orga-detail.component';

describe('LegalOrgaDetailComponent', () => {
  let component: LegalOrgaDetailComponent;
  let fixture: ComponentFixture<LegalOrgaDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegalOrgaDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalOrgaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
