import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LegalOrga, User} from 'app/shared/model/models';
import {TagInputModel} from 'app/shared/std-form-control/std-form-control.component';
import {ProfilesDataService} from 'app/shared/profiles-data.service';

interface EditMode {
  name: boolean;
  description: boolean;
  email: boolean;
  orga_admin: boolean;
  members: boolean;
  legal_form: boolean;
  tax_id: boolean;
  ust_id: boolean;
  iban: boolean;
  street: boolean;
  plz: boolean;
  city: boolean;
  phone: boolean;
  fax: boolean;
}

@Component({
  selector: 'app-legal-orga-detail',
  templateUrl: './legal-orga-detail.component.html',
  styleUrls: ['./legal-orga-detail.component.scss']
})

export class LegalOrgaDetailComponent implements OnInit, OnDestroy {
  user: User;
  users: User[];
  legalOrga: LegalOrga;
  subUser: Subscription;
  subUsers: Subscription;
  subLegalOrgas: Subscription;
  subRoute: Subscription;
  params: object;
  isAdmin: boolean;
  editMode: EditMode;
  legalOrgaForm: FormGroup;
  tagInputItems: TagInputModel[];
  tagInputMembers: TagInputModel[];
  tagInputAdmin: TagInputModel[];

  // TBD Jannis: This should be defined at another place. Where is to be determined :)
  legalOrgaForms = ['gbr', 'gmbh', 'ag', 'kg', 'gmbhcokg', 'eg', 'ev', 'pg', 'ohg', 'ug', 'st'];

  // is this a good way to import a js lib into ts?
  slugify: Function;

  get admin(): User {
    return this.users.find(user => {
      // loading hell in case new legalOrga
      if (this.legalOrga) {
        return this.legalOrga.orga_admin === user.url;
      } else {
        return false;
      }
    });
  }

  get members(): User[] {
    return this.users.filter(user => {
      return this.legalOrga.members.includes(user.url);
    });
  }

  constructor(private profilesDataService: ProfilesDataService, private route: ActivatedRoute, private fb: FormBuilder,
              private router: Router) {
    this.slugify = require('slugify');
    this.legalOrgaForm = this.createForm();
  }

  ngOnInit() {
    this.editMode = {
      name: false,
      description: false,
      email: false,
      orga_admin: false,
      members: false,
      legal_form: false,
      tax_id: false,
      ust_id: false,
      iban: false,
      street: false,
      plz: false,
      city: false,
      phone: false,
      fax: false,
    };

    this.subRoute = this.route.params.subscribe(params => {
      this.params = params;
    });
    this.subUsers = this.profilesDataService.users.subscribe(users => {
      this.users = users;
      this.tagInputItems = users.map((user): TagInputModel => {
        return {value: user.url, display: user.username};
      });
    });
    this.subLegalOrgas = this.profilesDataService.legalOrgas.subscribe(legalOrgas => {
      console.log(legalOrgas);
      this.legalOrga = legalOrgas.find(legalOrga => {
        const legalOrgaNameSlugParams = this.slugify(this.params['name']);
        const legalOrgaNameSlugState = this.slugify(legalOrga.name);
        return legalOrgaNameSlugParams === legalOrgaNameSlugState;
      });
      this.subUser = this.profilesDataService.auth.subscribe(auth => {
        this.user = auth.user;
        // loading hell in case new legalOrga
        if (this.admin) {
          this.isAdmin = auth.user.url === this.admin.url;
        } else {
          this.isAdmin = true;
        }
      });
      // if not existing create new
      if (!this.legalOrga) {
        this.legalOrga = {
          name: this.params['name'],
          description: undefined,
          email: 'bitte@ausfüllen.local',
          orga_admin: this.user.url,
          members: [this.user.url, ],
          legal_form: 'gbr',
          tax_id: 'Bitte ausfüllen',
          ust_id: undefined,
          iban: 'DE48123456783445',
          street: 'Bitte ausfüllen',
          plz: '12345',
          city: 'Bitte ausfüllen',
          phone: '+49123456',
          fax: undefined,
        };

        console.log(this.legalOrga.name);
      }
      this.legalOrgaForm.setValue({
        name: this.legalOrga.name,
        description: this.legalOrga.description || '',
        email: this.legalOrga.email || '',
        orga_admin: this.legalOrga.orga_admin || '',
        members: this.legalOrga.members || '',
        legal_form: this.legalOrga.legal_form || '',
        tax_id: this.legalOrga.tax_id || '',
        ust_id: this.legalOrga.ust_id || '',
        iban: this.legalOrga.iban || '',
        street: this.legalOrga.street || '',
        plz: this.legalOrga.plz || '',
        city: this.legalOrga.city || '',
        phone: this.legalOrga.phone || '',
        fax: this.legalOrga.fax || '',
      });
      this.tagInputMembers = this.members.map(user => {
        return {value: user.url, display: user.username};
      });
      this.tagInputAdmin = [{value: this.admin.url, display: this.admin.username}];

    });
  }

  /* TBD add validators */
  private createForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      description: ['', [Validators.maxLength(200)]],
      email: ['', [Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]],
      orga_admin: ['', [Validators.required,
        Validators.pattern(/^(http|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~‌​+#-]*[\w@?^=%&amp;\/‌​~+#-])?$/)]],
      members: ['', ],
      legal_form: ['', ],
      tax_id: ['', ],
      ust_id: ['', ],
      iban: ['', ],
      street: ['', ],
      plz: ['', ],
      city: ['', ],
      phone: ['', ],
      fax: ['', ],
    });
  }

  setTagInputMembers(tagInputMembers: TagInputModel[]) {
    const members = tagInputMembers.map(member => member.value);
    this.legalOrgaForm.patchValue({members: members});
    this.legalOrgaForm.controls['members'].markAsDirty();
  }

  setTagInputAdmin(tagInputMembers: TagInputModel[]) {
    const admin = tagInputMembers[0];
    if (admin) {
      this.legalOrgaForm.patchValue({orga_admin: admin.value});
      this.legalOrgaForm.controls['orga_admin'].markAsDirty();
    }
  }

  setFormValues(control: string) {
    if (this.legalOrgaForm.get(control).errors) {
      this.profilesDataService.raiseException(`Ungültige Eingabe in ${control} Feld`, 'error');
      return true;
    }
    this.profilesDataService.clearException();
    const legalOrga: LegalOrga = {...this.legalOrga};
    if (!this.legalOrgaForm.controls[control].pristine) {
      legalOrga[control] = this.legalOrgaForm.controls[control].value;
      // create or update?
      if (legalOrga.url) {
        this.profilesDataService.updateLegalOrga(legalOrga);
        // redirect to correct page if the name was just updated
        if (this.legalOrgaForm.controls['name'].value !== this.legalOrga.name) {
          this.router.navigate(['/app/legal-orga/' + this.legalOrgaForm.controls['name'].value]);
        }
      } else {
        this.profilesDataService.createLegalOrga(legalOrga);
      }
    }
    this.editMode[control] = false;
    this.legalOrgaForm.controls[control].markAsPristine();
  }

  resetFormValues(control: string) {
    switch (control) {
      case 'orga_admin':
        break;
      case 'members':
        break;
      default:
        this.legalOrgaForm.patchValue({[control]: this.legalOrga[control]});
    }
    this.editMode[control] = false;
    this.profilesDataService.clearException();
  }

  deleteLegalOrga() {
    this.profilesDataService.deleteLegalOrga(this.legalOrga);
    this.router.navigate(['/app/legal-orgas']);
  }

  ngOnDestroy() {
    this.subUser.unsubscribe();
    this.subUsers.unsubscribe();
    this.subLegalOrgas.unsubscribe();
  }

}
