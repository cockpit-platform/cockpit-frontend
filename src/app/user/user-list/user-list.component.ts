import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, Observable, Subject } from 'rxjs';
import { first, debounceTime, distinctUntilChanged, map, startWith } from 'rxjs/operators';
import {ProfilesDataService} from 'app/shared/profiles-data.service';
import {Collective, LegalOrga, Project, User} from 'app/shared/model/models';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {
  users: User[];
  users$: Observable<User[]>;
  collectives: Collective[];
  legalOrgas: LegalOrga[];
  projects: Project[];
  subUsers: Subscription;
  subCollectives: Subscription;
  subLegalOrgas: Subscription;
  subProjects: Subscription;
  searchSubject = new Subject<string>();

  constructor(private profilesDataService: ProfilesDataService) {
  }

  ngOnInit() {
    this.profilesDataService.users.pipe(first()).subscribe(s => {
      // avoid error here if we miss auth jet
      // if (!s) {return}
      // load user details if not jet existing
      s.map((user) => {
        if (!user.street) {
          this.profilesDataService.loadUserDetails(user.url);
        }
      });
    });
    this.subUsers = this.profilesDataService.users.subscribe(s => this.users = s);
    this.subCollectives = this.profilesDataService.collectives.subscribe(s => {
      // avoid error here if we miss auth jet
      // if (!s) {return}
      this.collectives = s;
    });
    this.subLegalOrgas = this.profilesDataService.legalOrgas.subscribe(s => {
      // avoid error here if we miss auth jet
      // if (!s) {return}
      this.legalOrgas = s;
    });
    this.subProjects = this.profilesDataService.projects.subscribe(s => {
      // avoid error here if we miss auth jet
      // if (!s) {return}
      this.projects = s;
    });

    // Filter
    this.users$ = this.searchSubject.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      map(term => term.length < 2 ? this.users
        : this.users.filter(v => [v.username, v.first_name, v.last_name, v.email].some(s => s.toLowerCase().indexOf(term.toLowerCase()) > -1))
          .slice(0, 10)),
      startWith(this.users)
    );
  }
  ngOnDestroy() {
    this.subUsers.unsubscribe();
    this.subCollectives.unsubscribe();
    this.subLegalOrgas.unsubscribe();
    this.subProjects.unsubscribe();
  }

  searchChanged(search: string) {
    this.searchSubject.next(search);
  }

}
