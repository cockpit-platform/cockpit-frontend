import {Route} from '@angular/router';
import {UserListComponent} from 'app/user/user-list/user-list.component';
import {ProfileComponent} from 'app/user/profile/profile.component';
import {UserContainerComponent} from 'app/user/user-container/user-container.component';
import {AppContainerComponent} from 'app/app-container/app-container.component';
import {AuthGuard} from 'app/shared/profiles-data.service';

export const USERS_ROUTE: Route = {
  path: 'app',
  component: AppContainerComponent,
  canActivate: [AuthGuard],
  children: [{
    path: 'users',
    component: UserListComponent
  },
    {
      path: 'users/:username',
      component: ProfileComponent
    }, ]
};
