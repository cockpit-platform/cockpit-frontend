import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {UserEntryComponent} from 'app/user/user-entry/user-entry.component';
import {UserListComponent} from 'app/user/user-list/user-list.component';
import {CockpitSharedModule} from 'app/shared/shared.module';
import {RouterModule} from '@angular/router';
import {ProfileComponent} from 'app/user/profile/profile.component';
import {USERS_ROUTE} from 'app/user/user.route';
import { UserContainerComponent } from './user-container/user-container.component';
import {CockpitPluginModule} from 'app/plugin/plugin.module';
import {MatCardModule} from '@angular/material/card';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  imports: [
    CockpitSharedModule.forRoot(),
    RouterModule.forChild([USERS_ROUTE]),
    CockpitPluginModule,
    FlexLayoutModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule],
  declarations: [UserEntryComponent, UserListComponent, ProfileComponent, UserContainerComponent],
  exports: [UserEntryComponent, UserListComponent, ProfileComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CockpitUserModule {
  static forRoot() {
    return {
      ngModule: CockpitUserModule
    };
  }
}
