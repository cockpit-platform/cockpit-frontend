import { Component, Input, OnInit } from '@angular/core';
import {Collective, LegalOrga, Project, User} from 'app/shared/model/models';
import {ProfilesDataService} from 'app/shared/profiles-data.service';

@Component({
  selector: 'app-user-entry',
  templateUrl: './user-entry.component.html',
  styleUrls: ['./user-entry.component.scss']
})
export class UserEntryComponent implements OnInit {
  @Input()
  user: User;

  @Input()
  collectives: Collective[];

  @Input()
  legalOrgas: LegalOrga[];

  @Input()
  projects: Project[];

  constructor(private profilesDataService: ProfilesDataService) { }

  ngOnInit() {
  }

  getCollectives(): Collective[] {
    return this.collectives.filter(entry => entry.members.includes(this.user.url));
  }

  getLegalOrgas(): LegalOrga[] {
    return this.legalOrgas.filter(entry => entry.members.includes(this.user.url));
  }

  getProjects(): Project[] {
    return this.projects.filter(entry => entry.members.includes(this.user.url));
  }

}
