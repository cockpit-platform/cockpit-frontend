import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { isUndefined } from 'util';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import {Collective, LegalOrga, Project, User} from 'app/shared/model/models';
import {ProfilesDataService} from 'app/shared/profiles-data.service';

interface EditMode {
  short_intro: boolean;
  email: boolean;
  phone_office: boolean;
  phone_mobile: boolean;
  phone_land: boolean;
  jabber: boolean;
  first_name: boolean;
  last_name: boolean;
  street: boolean;
  plz: boolean;
  city: boolean;
  is_freelancer: boolean;
  iban: boolean;
  tax_id: boolean;
  password: boolean;
}

// stackoverflow piece
export function matchOtherValidator(otherControlName: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const otherControl: AbstractControl = control.root.get(otherControlName);

    if (otherControl) {
      const subscription: Subscription = otherControl
        .valueChanges
        .subscribe(() => {
          control.updateValueAndValidity();
          subscription.unsubscribe();
        });
    }

    return (otherControl && control.value !== otherControl.value) ? { match: true } : null;
  };
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  params: object;
  user: User;
  collectives: Collective[];
  legalOrgas: LegalOrga[];
  projects: Project[];
  isMe: boolean;
  subRoute: Subscription;
  subUsers: Subscription;
  subCollectives: Subscription;
  subLegalOrgas: Subscription;
  subProjects: Subscription;
  subAuth: Subscription;
  editMode: EditMode;
  userForm = new FormGroup({
    short_intro: new FormControl('', [
      Validators.maxLength(500)
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
    ]),
    phone_office: new FormControl(),
    phone_mobile: new FormControl(),
    phone_land: new FormControl(),
    jabber: new FormControl('', [
      Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
    ]),
    first_name: new FormControl(),
    last_name: new FormControl(),
    street: new FormControl(),
    plz: new FormControl('', [
      Validators.maxLength(5),
      Validators.pattern(/^\d{5}$/)
    ]),
    city: new FormControl(),
    is_freelancer: new FormControl(),
    iban: new FormControl(),
    tax_id: new FormControl(),
    password: new FormControl('', [
      Validators.required, Validators.minLength(8)
    ]),
    password2: new FormControl('', [
      Validators.required, matchOtherValidator('password')
    ]),
  });

  constructor(private route: ActivatedRoute, private profilesDataService: ProfilesDataService, private router: Router) {
  }

  ngOnInit() {
    this.editMode = {
      short_intro: false,
      email: false,
      phone_office: false,
      phone_mobile: false,
      phone_land: false,
      jabber: false,
      first_name: false,
      last_name: false,
      street: false,
      plz: false,
      city: false,
      is_freelancer: false,
      iban: false,
      tax_id: false,
      password: false,
    };
    this.subUsers = combineLatest([this.profilesDataService.users, this.profilesDataService.auth, this.route.params]).subscribe(val => {
      const users = val[0];
      const auth = val[1];
      const params = val[2];
      this.params = params;
      if (users.length > 0) {
        this.user = users.find(user => {
          return user.username === this.params['username'];
        });
        this.isMe = auth.user.url === this.user.url;
        const shortIntro = (): string => {
          if (this.user.unstructured_info) {
            return this.user.unstructured_info['short_intro'] || '';
          } else {return '';}
        };
        this.userForm.setValue({
          short_intro: shortIntro(),
          email: this.user.email,
          phone_office: this.user.phone_office || '',
          phone_mobile: this.user.phone_mobile || '',
          phone_land: this.user.phone_land || '',
          jabber: this.user.jabber || '',
          first_name: this.user.first_name || '',
          last_name: this.user.last_name || '',
          street: this.user.street || '',
          plz: this.user.plz || '',
          city: this.user.city || '',
          is_freelancer: this.user.is_freelancer || false,
          iban: this.user.iban || '',
          tax_id: this.user.tax_id || '',
          password: '',
          password2: ''
        });
        // if user not existing
        if (isUndefined(this.user)) {
          this.router.navigate(['***']);
          return;
        }
        // lazy load user detail
        if (!this.user.unstructured_info) {
          this.profilesDataService.loadUserDetails(this.user.url);
        }
      }
    }, error => {
      this.router.navigate(['***']);
    });
    this.subCollectives = this.profilesDataService.collectives.subscribe(collectives => this.collectives = collectives);
    this.subLegalOrgas = this.profilesDataService.legalOrgas.subscribe(legalOrgas => this.legalOrgas = legalOrgas);
    this.subProjects = this.profilesDataService.projects.subscribe(projects => this.projects = projects);
  }

  getCollectives(): Collective[] {
    if (this.user) {
      return this.collectives.filter(entry => entry.members.includes(this.user.url));
    }
  }

  getLegalOrgas(): LegalOrga[] {
    if (this.user) {
      return this.legalOrgas.filter(entry => entry.members.includes(this.user.url));
    }
  }

  getProjects(): Project[] {
    if (this.user) {
      return this.projects.filter(entry => entry.members.includes(this.user.url));
    }
  }

  setFormValues(control: string) {
    if (this.userForm.get(control).errors) {
      this.profilesDataService.raiseException(`Ungültige Eingabe in ${control} Feld`, 'error');
      return true;
    }
    this.profilesDataService.clearException();
    const user = { ... this.user };
    const short_intro = (value: string) => {
      if (user.unstructured_info) {
        user.unstructured_info['short_intro'] = value;
      } else {
        user.unstructured_info = { 'short_intro': value };
      }
    };
    if (!this.userForm.controls[control].pristine) {
      switch (control) {
        case 'short_intro':
          short_intro(this.userForm.controls[control].value);
          break;
        case 'password':
          if (!this.userForm.controls['password2'].errors) {
            user['password'] = this.userForm.controls[control].value;
          } else {
            this.profilesDataService.raiseException(`Passwörter stimmen nicht überein.`, 'error');
            return;
          }
          break;
        default:
          user[control] = this.userForm.controls[control].value;
      }
      this.profilesDataService.updateUserDetails(user);
    }
    this.editMode[control] = false;
    this.userForm.controls[control].markAsPristine();
    // for (const key in this.userForm) {
    //   if (this.userForm.hasOwnProperty(key)) {
    //     if (!this.userForm[key].pristine) {
    //       switch (key) {
    //         case 'short_intro':
    //           short_intro(this.userForm[key].value);
    //           break;
    //         default:
    //           user[key] = this.userForm[key].value;
    //       }
    //     }
    //   }
    // }
  }

  resetFormValues(control: string) {
    switch (control) {
      case 'short_intro':
        const shortIntro = (): string => {
          if (this.user.unstructured_info) {
            return this.user.unstructured_info['short_intro'] || '';
          } else { return ''; }
        };
        this.userForm.patchValue({ short_intro: shortIntro() });
        break;
      case 'password':
        this.userForm.patchValue({ password: '', password2: '' });
        break;
      default:
        this.userForm.patchValue({ [control]: this.user[control] });
    }
    this.editMode[control] = false;
    this.profilesDataService.clearException();
  }

  ngOnDestroy() {
    this.subUsers.unsubscribe();
    this.subCollectives.unsubscribe();
    this.subLegalOrgas.unsubscribe();
    this.subProjects.unsubscribe();
  }

}
