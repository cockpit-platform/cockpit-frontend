import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { ProjectDetailComponent } from 'app/project/project-detail/project-detail.component';
import { ProjectListComponent } from 'app/project/project-list/project-list.component';
import { CockpitSharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { PROJECT_ROUTE } from 'app/project/project.route';
import {FlexLayoutModule} from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

@NgModule({
  imports: [
    CockpitSharedModule.forRoot(),
    RouterModule.forChild([PROJECT_ROUTE]),
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatSlideToggleModule
  ],
  declarations: [ProjectDetailComponent, ProjectListComponent],
  exports: [ProjectDetailComponent, ProjectListComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class CockpitProjectModule {
  static forRoot() {
    return {
      ngModule: CockpitProjectModule,
    };
  }
}
