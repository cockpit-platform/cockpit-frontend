import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, Subject, Observable } from 'rxjs';
import {Project} from 'app/shared/model/models';
import {ProfilesDataService} from 'app/shared/profiles-data.service';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit, OnDestroy {
  projects: Project[];
  allProjects: Project[];
  filteredProjects2: Project[];
  subProjects: Subscription;
  deactivatedProjectsFilterSubject = new Subject<boolean>();
  filteredProjects: Observable<Project[]>;

  constructor(private profilesDataService: ProfilesDataService) { }

  ngOnInit() {
    this.subProjects = this.profilesDataService.projects.subscribe(projects => {
      this.allProjects = projects;
      this.filteredProjects2 = this.allProjects.filter(project => !project.deactivated_timestamp);
      this.projects = this.filteredProjects2;
    } );
  }

  ngOnDestroy() {
    this.subProjects.unsubscribe();
  }

  changeDeactivatedProjectsFilter(event: MatSlideToggleChange) {
    if (event.checked) {
      this.projects = this.allProjects;
    } else {
      this.projects = this.filteredProjects2;
    }
    console.log(this.projects);
  }

}
