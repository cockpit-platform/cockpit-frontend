import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Project, User} from 'app/shared/model/models';
import {TagInputModel} from 'app/shared/std-form-control/std-form-control.component';
import {ProfilesDataService} from 'app/shared/profiles-data.service';
import {formatDate} from '@angular/common';

interface EditMode {
  name: boolean;
  description: boolean;
  email: boolean;
  members: boolean;
  orga_admin: boolean;
  deactivated_timestamp: boolean;
}

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit, OnDestroy {
  user: User;
  users: User[];
  project: Project;
  subUser: Subscription;
  subUsers: Subscription;
  subProjects: Subscription;
  subRoute: Subscription;
  params: object;
  isAdmin: boolean;
  editMode: EditMode;
  projectForm: FormGroup;
  tagInputItems: TagInputModel[];
  tagInputMembers: TagInputModel[];
  tagInputAdmin: TagInputModel[];

  // is this a good way to import a js lib into ts?
  slugify: Function;

  get admin(): User {
    return this.users.find(user => {
      // loading hell in case new project
      if (this.project) {
        return this.project.orga_admin === user.url;
      } else {
        return false;
      }
    });
  }

  get members(): User[] {
    return this.users.filter(user => {
      return this.project.members.includes(user.url);
    });
  }

  constructor(private profilesDataService: ProfilesDataService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router) {
    this.slugify = require('slugify');
    this.projectForm = this.createForm();
  }

  ngOnInit() {
    this.editMode = {
      name: false,
      description: false,
      email: false,
      members: false,
      orga_admin: false,
      deactivated_timestamp: false
    };

    this.subRoute = this.route.params.subscribe(params => {
      this.params = params;
    });

    this.subUsers = this.profilesDataService.users.subscribe(users => {
      this.users = users;
      this.tagInputItems = users.map((user): TagInputModel => {
        return {value: user.url, display: user.username};
      });
    });

    this.subProjects = this.profilesDataService.projects.subscribe(projects => {
      this.project = projects.find(project => {
        const projectNameSlugParams = this.slugify(this.params['name']);
        const projectNameSlugState = this.slugify(project.name);
        return projectNameSlugParams === projectNameSlugState;
      });

      this.subUser = this.profilesDataService.auth.subscribe(auth => {
        this.user = auth.user;
        // loading hell in case new project
        if (this.admin) {
          this.isAdmin = auth.user.url === this.admin.url;
        } else {
          this.isAdmin = true;
        }
      });

      // if not existing create new
      if (!this.project) {
        this.project = {
          name: this.params['name'],
          description: undefined,
          email: undefined,
          members: [this.user.url, ],
          orga_admin: this.user.url,
        };
        console.log(this.project.name);
      }

      this.projectForm.setValue({
        name: this.project.name,
        description: this.project.description || '',
        email: this.project.email || '',
        members: this.project.members || '',
        orga_admin: this.project.orga_admin || '',
        deactivated_timestamp: this.project.deactivated_timestamp || '',
      });

      this.tagInputMembers = this.members.map(user => {
        return {value: user.url, display: user.username};
      });

      this.tagInputAdmin = [{value: this.admin.url, display: this.admin.username}];

    });
  }

  private createForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      description: ['', [Validators.maxLength(200)]],
      email: ['', [Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]],
      members: ['', ],
      orga_admin: ['', [Validators.required,
        Validators.pattern(/^(http|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~‌​+#-]*[\w@?^=%&amp;\/‌​~+#-])?$/)]],
      deactivated_timestamp: ['']
    });
  }

  setTagInputMembers(tagInputMembers: TagInputModel[]) {
    const members = [];
    for (const member of tagInputMembers) {
      members.push(member.value);
    }
    this.projectForm.patchValue({members: members});
    this.projectForm.controls['members'].markAsDirty();
  }

  setTagInputAdmin(tagInputMembers: TagInputModel[]) {
    const admin = tagInputMembers[0];
    if (admin) {
      this.projectForm.patchValue({orga_admin: admin.value});
      this.projectForm.controls['orga_admin'].markAsDirty();
    }
  }

  setFormValues(control: string) {
    if (this.projectForm.get(control).errors) {
      this.profilesDataService.raiseException(`Ungültige Eingabe in ${control} Feld`, 'error');
      return true;
    }
    this.profilesDataService.clearException();
    const project: Project = {...this.project};
    if (!this.projectForm.controls[control].pristine) {
      if (control === 'deactivated_timestamp' && this.projectForm.controls[control].value !== null) {
        project[control] = formatDate(this.projectForm.controls[control].value, 'yyyy-MM-dd', 'en-US');
      } else {
        project[control] = this.projectForm.controls[control].value;
      }
      // create or update?
      if (project.url) {
        this.profilesDataService.updateProject(project);
        // redirect to correct page if the name was just updated
        if (this.projectForm.controls['name'].value !== this.project.name) {
          this.router.navigate(['/app/project/' + this.projectForm.controls['name'].value]);
        }
      } else {
        this.profilesDataService.createProject(project);
      }
    }
    this.editMode[control] = false;
    this.projectForm.controls[control].markAsPristine();
  }

  resetFormValues(control: string) {
    switch (control) {
      case 'orga_admin':
        break;
      case 'members':
        break;
      default:
        this.projectForm.patchValue({[control]: this.project[control]});
    }
    this.editMode[control] = false;
    this.profilesDataService.clearException();
  }

  deleteProject() {
    this.profilesDataService.deleteProject(this.project);
    this.router.navigate(['/app/projects']);
  }

  ngOnDestroy() {
    this.subUser.unsubscribe();
    this.subUsers.unsubscribe();
    this.subProjects.unsubscribe();
  }

}
