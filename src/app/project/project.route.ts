import {Route} from '@angular/router';
import {ProjectListComponent} from 'app/project/project-list/project-list.component';
import {ProjectDetailComponent} from 'app/project/project-detail/project-detail.component';
import {AppContainerComponent} from 'app/app-container/app-container.component';
import {AuthGuard} from 'app/shared/profiles-data.service';

export const PROJECT_ROUTE: Route = {
  path: 'app',
  component: AppContainerComponent,
  canActivate: [AuthGuard],
  children: [ {
    path: 'projects',
    component: ProjectListComponent,
  },
  {
    path: 'project/:name',
    component: ProjectDetailComponent
  }]
};
