import { BrowserModule } from '@angular/platform-browser';
import {
  APP_INITIALIZER,
  CUSTOM_ELEMENTS_SCHEMA,
  Injector,
  NgModule,
} from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JwtModule } from '@auth0/angular-jwt';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';

import { HeaderComponent } from './layout/header/header.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { PageNotFoundComponent } from './layout/page-not-found/page-not-found.component';
import { ExceptionNoticeComponent } from './layout/exception-notice/exception-notice.component';
import { LoginComponent } from './login/login.component';
import { AppContainerComponent } from './app-container/app-container.component';
import {
  AuthGuard,
  ProfilesDataService,
} from 'app/shared/profiles-data.service';
import { CockpitCollectiveModule } from 'app/collective/collective.module';
import { CockpitProjectModule } from 'app/project/project.module';
import { CockpitUserModule } from 'app/user/user.module';
import { CockpitLegalOrgaModule } from 'app/legal-orga/legal-orga.module';
import { CockpitDashboardModule } from 'app/dashboard/dashboard.module';
import { CockpitSharedModule } from 'app/shared/shared.module';
import { CockpitLoginModule } from 'app/login/login.module';
import { environment } from './../environments/environment';

// Plugins
import { forkJoin } from 'rxjs';
import { Router } from '@angular/router';
import {
  authReducer,
  collectiveReducer,
  exceptionReducer,
  legalOrgaReducer,
  projectReducer,
  userReducer,
} from 'app/shared/redux/reducers';
import { StoreModule } from '@ngrx/store';
import {CockpitPluginModule} from 'app/plugin/plugin.module';
import {SkillAvailabilityModule} from 'app/skill-availability/skill-availability.module';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatSnackBarModule} from '@angular/material/snack-bar';

export function tokenGetter(): string {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    ExceptionNoticeComponent,
    AppContainerComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    MatDividerModule,
    MatButtonModule,
    MatSnackBarModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        authScheme: 'JWT ',
        whitelistedDomains: [
          environment.jwtWhitelistDomain
        ],
        //      blacklistedRoutes: ['example.com/examplebadroute/']
      },
    }),
    CockpitCollectiveModule,
    CockpitProjectModule,
    CockpitUserModule,
    CockpitLegalOrgaModule,
    CockpitDashboardModule,
    AppRoutingModule,
    CockpitSharedModule.forRoot(),
    CockpitLoginModule,
    CockpitPluginModule,
    SkillAvailabilityModule
    // StoreModule.forRoot({
    //   users: userReducer,
    //   collectives: collectiveReducer,
    //   legalOrgas: legalOrgaReducer,
    //   projects: projectReducer,
    //   auth: authReducer,
    //   exception: exceptionReducer
    // }),
    //    AuthModule,
  ],
  providers: [
    AuthGuard,
    ProfilesDataService,
    {
      provide: APP_INITIALIZER,
      useFactory: onAppInit,
      multi: true,
      deps: [Injector],
    },
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}

export function onAppInit(injector: Injector): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise((resolve, reject) => {
      // we need to use injector because of cyclic dependencies
      const profilesDataService = injector.get(ProfilesDataService);
      const router = injector.get(Router);
      if (localStorage.getItem('token')) {
        profilesDataService.refreshAuth();
        const profilesLoad = forkJoin(
          profilesDataService.loadUsers(),
          profilesDataService.loadCollectives(),
          profilesDataService.loadLegalOrgas(),
          profilesDataService.loadProjects()
        ).subscribe(
          s => {
            if (s) {
              profilesLoad.unsubscribe();
              profilesDataService
                .refreshToken()
                .subscribe(token => console.log(token));
              resolve();
            }
          },
          s => {
            if (s) {
              profilesLoad.unsubscribe();
              resolve();
            }
          }
        );
      } else {
        resolve();
      }
    });
  };
}
