import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CollectiveDetailComponent} from 'app/collective/collective-detail/collective-detail.component';
import {CollectiveListComponent} from 'app/collective/collective-list/collective-list.component';
import {CockpitSharedModule} from 'app/shared/shared.module';
import {RouterModule} from '@angular/router';
import {COLLECTIVES_ROUTE} from 'app/collective/collective.route';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  imports: [CockpitSharedModule.forRoot(), RouterModule.forChild([COLLECTIVES_ROUTE]), FlexLayoutModule, MatButtonModule, MatIconModule],
  declarations: [CollectiveDetailComponent, CollectiveListComponent],
  exports: [CollectiveDetailComponent, CollectiveListComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CockpitCollectiveModule {
  static forRoot() {
    return {
      ngModule: CockpitCollectiveModule
    };
  }
}
