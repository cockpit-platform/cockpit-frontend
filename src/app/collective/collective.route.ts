import {Route} from '@angular/router';
import {CollectiveListComponent} from 'app/collective/collective-list/collective-list.component';
import {CollectiveDetailComponent} from 'app/collective/collective-detail/collective-detail.component';
import {AppContainerComponent} from 'app/app-container/app-container.component';
import {AuthGuard} from 'app/shared/profiles-data.service';

export const COLLECTIVES_ROUTE: Route = {
  path: 'app',
  component: AppContainerComponent,
  canActivate: [AuthGuard],
  children: [ {
    path: 'collectives',
    component: CollectiveListComponent,
  },
  {
    path: 'collective/:name',
    component: CollectiveDetailComponent
  }]
};
