import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import {ProfilesDataService} from 'app/shared/profiles-data.service';
import {Collective} from 'app/shared/model/models';

@Component({
  selector: 'app-collective-list',
  templateUrl: './collective-list.component.html',
  styleUrls: ['./collective-list.component.scss']
})
export class CollectiveListComponent implements OnInit, OnDestroy {
  collectives: Collective[];
  subCollectives: Subscription;

  constructor(private profilesDataService: ProfilesDataService) { }

  ngOnInit() {
    this.subCollectives = this.profilesDataService.collectives.subscribe(collectives => {
      this.collectives = collectives;
    });
  }

  ngOnDestroy() {
    this.subCollectives.unsubscribe();
  }
}
