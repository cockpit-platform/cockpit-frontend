import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Collective, User} from 'app/shared/model/models';
import {TagInputModel} from 'app/shared/std-form-control/std-form-control.component';
import {ProfilesDataService} from 'app/shared/profiles-data.service';
// import * as slugify from 'slugify';

interface EditMode {
  name: boolean;
  description: boolean;
  email: boolean;
  orga_admin: boolean;
  members: boolean;
}

@Component({
  selector: 'app-collective-detail',
  templateUrl: './collective-detail.component.html',
  styleUrls: ['./collective-detail.component.scss']
})
export class CollectiveDetailComponent implements OnInit, OnDestroy {
  user: User;
  users: User[];
  collective: Collective;
  subUser: Subscription;
  subUsers: Subscription;
  subCollectives: Subscription;
  subRoute: Subscription;
  params: object;
  isAdmin: boolean;
  editMode: EditMode;
  collectiveForm: FormGroup;
  tagInputItems: TagInputModel[];
  tagInputMembers: TagInputModel[];
  tagInputAdmin: TagInputModel[];

  // is this a good way to import a js lib into ts?
  slugify: Function;

  get admin(): User {
    return this.users.find(user => {
      // loading hell in case new collective
      if (this.collective) {
        return this.collective.orga_admin === user.url;
      } else { return false; }
    });
  }

  get members(): User[] {
    return this.users.filter(user => {
      return this.collective.members.includes(user.url);
    });
  }

  // getter and setter do not work on ngmodel. Not even so knows why
  // get tagInputMembers(): TagInputModel[] {
  //   console.log(this.members.map(user => {return {value: user.url, display: user.username}}));
  //   return this.members.map(user => {return {value: user.url, display: user.username}})
  // }
  //
  // set tagInputMembers(tagInputMembers: TagInputModel[]) {
  //   const members = [];
  //   for (const member of tagInputMembers) {
  //     members.push(member.value)
  //   }
  //   this.collectiveForm.patchValue({members: members});
  // }

  constructor(private profilesDataService: ProfilesDataService, private route: ActivatedRoute, private fb: FormBuilder,
    private router: Router) {
    this.slugify = require('slugify');
    this.collectiveForm = this.createForm();
  }

  ngOnInit() {
    this.editMode = {
      name: false,
      description: false,
      email: false,
      orga_admin: false,
      members: false,
    };
    this.subRoute = this.route.params.subscribe(params => {
      this.params = params;
    });
    this.subUsers = this.profilesDataService.users.subscribe(users => {
      this.users = users;
      this.tagInputItems = users.map((user): TagInputModel => ({ value: user.url, display: user.username }));
    });
    this.subCollectives = this.profilesDataService.collectives.subscribe(collectives => {
      this.collective = collectives.find(collective => {
        const collectiveNameSlugParams = this.slugify(this.params['name']);
        const collectiveNameSlugState = this.slugify(collective.name);
        return collectiveNameSlugParams === collectiveNameSlugState;
      });
      this.subUser = this.profilesDataService.auth.subscribe(auth => {
        this.user = auth.user;
        // loading hell in case new collective
        if (this.admin) {
          this.isAdmin = auth.user.url === this.admin.url;
        } else {
          this.isAdmin = true;
        }
      });
      // if not existing create new
      if (!this.collective) {
        this.collective = {
          name: this.params['name'],
          description: undefined,
          email: undefined,
          orga_admin: this.user.url,
          members: [this.user.url, ],
          phone: undefined,
          street: undefined,
          legal_form: undefined,
          iban: undefined
        };
        console.log(this.collective.name);
      }
      this.collectiveForm.setValue({
        name: this.collective.name,
        email: this.collective.email || '',
        description: this.collective.description || '',
        orga_admin: this.collective.orga_admin || '',
        members: this.collective.members || '',
      });
      this.tagInputMembers = this.members.map(user => ({ value: user.url, display: user.username }));
      this.tagInputAdmin = [{ value: this.admin.url, display: this.admin.username }];

    });
  }

  private createForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      description: ['', [Validators.maxLength(200)]],
      email: ['', [Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]],
      orga_admin: ['', [Validators.required,
      Validators.pattern(/^(http|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~‌​+#-]*[\w@?^=%&amp;\/‌​~+#-])?$/)]],
      members: ['', ]
    });
  }

  setTagInputMembers(tagInputMembers: TagInputModel[]) {
    const members = [];
    for (const member of tagInputMembers) {
      members.push(member.value);
    }
    this.collectiveForm.patchValue({ members: members });
    this.collectiveForm.controls['members'].markAsDirty();
  }

  setTagInputAdmin(tagInputMembers: TagInputModel[]) {
    const admin = tagInputMembers[0];
    if (admin) {
      this.collectiveForm.patchValue({ orga_admin: admin.value });
      this.collectiveForm.controls['orga_admin'].markAsDirty();
    }
  }

  setFormValues(control: string) {
    if (this.collectiveForm.get(control).errors) {
      this.profilesDataService.raiseException(`Ungültige Eingabe in ${control} Feld`, 'error');
      return true;
    }
    this.profilesDataService.clearException();
    const collective: Collective = { ... this.collective };
    if (!this.collectiveForm.controls[control].pristine) {
      collective[control] = this.collectiveForm.controls[control].value;
      // create or update?
      if (collective.url) {
        this.profilesDataService.updateCollective(collective);
        // redirect to correct page if the name was just updated
        if (this.collectiveForm.controls['name'].value !== this.collective.name) {
          this.router.navigate(['/app/collective/' + this.collectiveForm.controls['name'].value]);
        }
      } else {
        this.profilesDataService.createCollective(collective);
      }
    }
    this.editMode[control] = false;
    this.collectiveForm.controls[control].markAsPristine();
  }

  resetFormValues(control: string) {
    switch (control) {
      case 'orga_admin':
        break;
      case 'members':
        break;
      default:
        this.collectiveForm.patchValue({ [control]: this.collective[control] });
    }
    this.editMode[control] = false;
    this.profilesDataService.clearException();
  }

  deleteCollective() {
    this.profilesDataService.deleteCollective(this.collective);
    this.router.navigate(['/app/collectives']);
  }

  ngOnDestroy() {
    this.subUser.unsubscribe();
    this.subUsers.unsubscribe();
    this.subCollectives.unsubscribe();
  }

}
