import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import {ProfilesDataService} from 'app/shared/profiles-data.service';

@Component({
  selector: 'app-app-container',
  templateUrl: './app-container.component.html',
  styleUrls: ['./app-container.component.scss']
})
export class AppContainerComponent implements OnInit, OnDestroy {

  constructor(private profilesDataService: ProfilesDataService) { }

  // loadInitialState() {
  //   this.profilesDataService.loadUsers();
  //   this.profilesDataService.refreshAuth();
  //   this.profilesDataService.loadCollectives();
  //   this.profilesDataService.loadLegalOrgas();
  //   this.profilesDataService.loadProjects();
  // }

  ngOnInit() {
    // this.loadInitialState();
  }

  ngOnDestroy() {
  }
}
