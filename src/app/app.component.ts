import { Component, AfterViewInit } from '@angular/core';
import {Store} from '@ngrx/store';
import { PluginService } from './plugin/plugin-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'cockpit-frontend';

  constructor(private pluginService: PluginService) {}
  ngAfterViewInit() {
    this.pluginService.loadPlugins();
  }
}
