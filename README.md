# Cockpit-Frontend

This is the frontend project of the cockpit project. It is based on [Angular](https://angular.io) and [Material UI components](https://material.angular.io).

This is still very much work in progress.

![](screenshot.png)

## Setup

You have to change the URLs in src/environments/environment{.prod}.ts to the URL where cockpit-backend is running.

## Development

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

### Getting started

1. install npm in your distribution package management

e.g.: `sudo apt install npm`

2. update your node version:

```
npm cache clean -f
npm install -g n
n stable
```

3. install project package requirements

`npm install` (in project directory)

### Run Frontend locally

As we call an API on a different host, we are facing cors issues in development. To be able to develop and test anyway you need to deactivate security:

```bash
chromium-browser --disable-web-security --user-data-dir=/tmp
```

To test in Firefox you would need a way not only not to check cors, but also prevent the browser to send OPTIONS requests. Feel free to commit a solution.

## Develop a plugin

### Some words on projects modularity

Cockpit Frontend features the same modularity as the projects backend. The core module is working on the "profiles"-api. All other features are implemented as plugins, which live in the same codebase, but can get activated and deactivated as needed.

There was a lot of work on different approaches to implement the plugin system as Angular changed from SystemJS/JIT compilation to webpack/AOT. An approach to make Plugins runtime plugable turned out to be very complicated in terms of implementation, build and plugin development. Furthermore no solution was found to inject angular services at runtime.

Finally it was decided to make the Application only pluggable at compile time. This puts a little more load on System-Administrators as they need to choose their needed moduls both in frontend and backend and then build their JS-bundle on their own.

Plugins are special Angular moduls which need to be included in `app.module.ts` as regular moduls. That way plugins have access on all features and singleton injectables of the core application and can extend the application in terms of routes and services. They are compiled together with the App and can only build within their appropriate position in the `app`-folder.

Plugins can offer the following:
- inject components at plugin hooks
- extend the application state by implementing their own reducers
- append their own routes

`plugin1` can be used as a reference implementation for building a new plugin.


### Getting started

1. Duplicate the folder "plugin1" and give it a proper name for your plugin. This name will be used for building the plugin as well as for the plugins URLs root path.


1. Implement your plugin. You can find out how this is done by checking the reference code of "plugin1" for plugin hooks, routes and state.

### Plugin routes

Plugin routes should live under `app/<my-plugin-name-slug>/<my-route>`. By haveing their own namespace we avoid a possible clash of routes.


